import 'package:flutter/material.dart';

class TextWithIcon extends StatelessWidget {
  const TextWithIcon({
    Key key,
    this.onPressed,
    @required this.label,
  }) : super(key: key);

  final String label;
  final dynamic onPressed;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        label == null
            ? SizedBox()
            : Text(label,
                textAlign: TextAlign.center,
                style: new TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.w600,
                )),
        onPressed == null
            ? SizedBox(height: 30.0)
            : IconButton(
                onPressed: onPressed(),
                iconSize: 30.0,
                icon: Icon(Icons.arrow_forward, color: Colors.black)),
      ],
    );
  }
}
