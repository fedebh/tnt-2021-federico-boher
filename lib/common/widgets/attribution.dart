import 'package:flutter/material.dart';

class Attribution extends StatelessWidget {
  const Attribution(this.attribution, {Key key}) : super(key: key);

  final String attribution;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 10.0, bottom: 45.0),
      child: Text(
        this.attribution,
        textAlign: TextAlign.center,
      ),
    );
  }
}
