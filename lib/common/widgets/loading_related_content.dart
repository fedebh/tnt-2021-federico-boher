import 'package:flutter/material.dart';

class LoadingRelatedContent extends StatelessWidget {
  const LoadingRelatedContent({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 80,
      child: Center(child: CircularProgressIndicator())
    );
  }
}