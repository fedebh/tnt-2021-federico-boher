import 'package:flutter/material.dart';

snackBarWidget(text) {
  return SnackBar(
    content: Text(text, textAlign: TextAlign.center),
    behavior: SnackBarBehavior.floating,
    shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(20))),
  );
}