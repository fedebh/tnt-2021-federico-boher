import 'package:actividad_05/common/widgets/text_with_icon.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'anime_card.dart';

class AnimeCardList extends StatelessWidget {
  const AnimeCardList(
      {Key key,
      this.label,
      this.maxLength,
      this.list,
      this.onTapImage,
      this.onPressedIcon})
      : super(key: key);

  final String label;
  final List<dynamic> list;
  final Function onTapImage;
  final Function onPressedIcon;
  final int maxLength;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        onPressedIcon == null
            ? TextWithIcon(label: label)
            : TextWithIcon(
                label: label,
                onPressed: () => onPressedIcon,
              ),
        Container(
          height: 240,
          child: ListView.builder(
              scrollDirection: Axis.horizontal,
              itemCount: maxLength != null && list.length > maxLength
                  ? maxLength
                  : list.length,
              itemBuilder: (context, index) {
                return GestureDetector(
                    onTap: () => onTapImage(index),
                    child: AnimeCard(list[index]));
              }),
        ),
      ],
    );
  }
}
