import 'package:actividad_05/common/widgets/anime/anime_card\.dart';
import 'package:flutter/material.dart';

class AnimeCardGrid extends StatelessWidget {
  const AnimeCardGrid({
    Key key,
    this.label,
    this.list,
    this.onTapImage,
    this.onBeginReached,
    this.onEndReached,
    this.scrollController,
  }) : super(key: key);

  final String label;
  final List<dynamic> list;
  final Function onTapImage;
  final Function onBeginReached;
  final Function onEndReached;
  final ScrollController scrollController;

  @override
  Widget build(BuildContext context) {
    final List<dynamic> _list = list != null ? list : [];
    final _scrollController = scrollController ?? new ScrollController();
    final Function _onBeginReached = onBeginReached ?? () => {};
    final Function _onEndReached = onEndReached ?? (_) => {};
    final Function _onTapImage = onTapImage ?? (_) => {};

    _scrollController.addListener(() {
      final double currentScrollPosition = _scrollController.position.pixels;
      if (currentScrollPosition == _scrollController.position.maxScrollExtent) {
        _onEndReached(currentScrollPosition);
      } else if (currentScrollPosition ==
          _scrollController.position.minScrollExtent) {
        _onBeginReached();
      }
    });

    return Container(
        child: Scrollbar(
            child: GridView.builder(
                scrollDirection: Axis.vertical,
                itemCount: _list.length,
                controller: _scrollController,
                gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                  maxCrossAxisExtent: 200,
                  mainAxisSpacing: 0,
                  crossAxisSpacing: 0,
                  childAspectRatio: AnimeCard.CARD_ASPECT_RATIO,
                ),
                itemBuilder: (BuildContext context, int index) =>
                    GestureDetector(
                      onTap: () => _onTapImage(index),
                      child: Container(
                          margin: EdgeInsets.only(right: 15),
                          child: AnimeCard(_list[index])),
                    ))));
  }
}
