import 'package:actividad_05/models/anime/anime.dart';
import 'package:flutter/material.dart';

class AnimeCard extends StatelessWidget {
  AnimeCard(this.anime, {Key key}) : super(key: key);

  final Anime anime;

  static const double CARD_WIDTH = 150;
  static const double CARD_HEIGHT = 200;
  static const double CARD_ASPECT_RATIO = CARD_WIDTH / CARD_HEIGHT;

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
      Flexible(
      flex: 1,
      child: Container(
        height: CARD_HEIGHT,
        width: CARD_WIDTH,
        margin: EdgeInsets.only(left: 15, bottom: 10, top: 5),
        decoration: BoxDecoration(
          boxShadow: [
            BoxShadow(
                offset: Offset(0, 4),
                blurRadius: 6,
                color: Colors.black54.withOpacity(0.25))
          ],
          image: DecorationImage(
            image: NetworkImage(anime.coverImage.large),
            fit: BoxFit.cover,
          ),
          borderRadius: BorderRadius.circular(5),
        ),
      )),
      Container(
          margin: EdgeInsets.only(left: 15, bottom: 10),
          width: CARD_WIDTH,
          child: Text(
            anime.title.userPreferred,
            style: TextStyle(
              fontSize: 12,
              fontWeight: FontWeight.w600,
              color: Colors.grey[600],
            ),
            textAlign: TextAlign.left,
            overflow: TextOverflow.ellipsis,
          )),
    ]);
  }
}
