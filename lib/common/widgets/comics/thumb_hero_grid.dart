import 'package:actividad_05/common/widgets/comics/thumb_hero.dart';
import 'package:actividad_05/models/comics/thumbnail.dart';
import 'package:flutter/material.dart';

class ThumbHeroGrid extends StatelessWidget {
  ThumbHeroGrid({
    Key key,
    this.label,
    this.list,
    this.onTapImage,
    this.onBeginReached,
    this.onEndReached,
    this.scrollController,
  }) : super(key: key);

  final String label;
  final List<dynamic> list;
  final Function onTapImage;
  final Function onBeginReached;
  final Function onEndReached;
  final ScrollController scrollController;

  final double _comicsAspectRatio =
      thumbnailSizeDimensions[ThumbnailSize.PORTRAIT_MEDIUM].width /
          thumbnailSizeDimensions[ThumbnailSize.PORTRAIT_MEDIUM].height;

  @override
  Widget build(BuildContext context) {
    final List<Thumbnail> _thumbnailList = list != null
        ? list.map<Thumbnail>((item) => item.thumbnail).toList()
        : [];
    final _scrollController = scrollController ?? new ScrollController();
    final Function _onBeginReached = onBeginReached ?? () => {};
    final Function _onEndReached = onEndReached ?? (_) => {};
    final Function _onTapImage = onTapImage ?? (_) => {};

    _scrollController.addListener(() {
      final double currentScrollPosition = _scrollController.position.pixels;
      if (currentScrollPosition == _scrollController.position.maxScrollExtent) {
        _onEndReached(currentScrollPosition);
      } else if (currentScrollPosition ==
          _scrollController.position.minScrollExtent) {
        _onBeginReached();
      }
    });

    return Container(
        child: Scrollbar(
            child: GridView.builder(
                scrollDirection: Axis.vertical,
                itemCount: _thumbnailList.length,
                controller: _scrollController,
                gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                    maxCrossAxisExtent: 200,
                    childAspectRatio: _comicsAspectRatio,
                    crossAxisSpacing: 0,
                    mainAxisSpacing: 0),
                itemBuilder: (BuildContext context, int index) =>
                    GestureDetector(
                        onTap: () => _onTapImage(index),
                        child: ThumbHero(_thumbnailList[index])))));
  }
}
