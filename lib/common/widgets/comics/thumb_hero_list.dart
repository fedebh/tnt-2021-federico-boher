import 'package:actividad_05/common/widgets/text_with_icon.dart';
import 'package:actividad_05/models/comics/thumbnail.dart';
import 'package:flutter/material.dart';
import 'thumb_hero.dart';

class ThumbHeroList extends StatelessWidget {
  const ThumbHeroList(
      {Key key,
      this.label,
      this.maxLength,
      this.list,
      this.onTapImage,
      this.onPressedIcon})
      : super(key: key);

  final String label;
  final List<dynamic> list;
  final Function onTapImage;
  final Function onPressedIcon;
  final int maxLength;

  @override
  Widget build(BuildContext context) {
    final List<Thumbnail> _thumbnailList =
        list.map<Thumbnail>((item) => item.thumbnail).toList();

    return Column(
      children: <Widget>[
        onPressedIcon == null
            ? TextWithIcon(label: label)
            : TextWithIcon(label: label, onPressed: () => onPressedIcon),
        Container(
            height:
                thumbnailSizeDimensions[ThumbnailSize.PORTRAIT_MEDIUM].height,
            child: ListView.builder(
                scrollDirection: Axis.horizontal,
                itemCount:
                    maxLength != null && _thumbnailList.length > maxLength
                        ? maxLength
                        : _thumbnailList.length,
                itemBuilder: (BuildContext context, int index) {
                  return GestureDetector(
                      onTap: () => onTapImage(index),
                      child: ThumbHero(_thumbnailList[index]));
                }))
      ],
    );
  }
}
