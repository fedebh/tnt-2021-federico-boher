import 'package:actividad_05/models/comics/thumbnail.dart';
import 'package:flutter/material.dart';

class ThumbHero extends StatelessWidget {
  const ThumbHero(this.thumb, {Key key}) : super(key: key);

  final Thumbnail thumb;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: thumbnailSizeDimensions[ThumbnailSize.PORTRAIT_MEDIUM].width,
      height: thumbnailSizeDimensions[ThumbnailSize.PORTRAIT_MEDIUM].height,
      margin: EdgeInsets.all(10),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(10),
        boxShadow: [
          BoxShadow(
            color: Colors.black54,
            blurRadius: 6,
            offset: Offset(0, 4),
          ),
        ],
        image: DecorationImage(
          fit: BoxFit.cover,
          image: NetworkImage(
              this.thumb.getSizedThumb(ThumbnailSize.PORTRAIT_UNCANNY)),
        ),
      ),
    );
  }
}
