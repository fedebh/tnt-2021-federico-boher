import 'package:actividad_05/common/widgets/attribution.dart';
import 'package:actividad_05/repositories/marvel_repository.dart';
import 'package:flutter/material.dart';

class MarvelAttribution extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Attribution(MarvelRepository.MARVEL_ATTRIBUTION);
  }
}
