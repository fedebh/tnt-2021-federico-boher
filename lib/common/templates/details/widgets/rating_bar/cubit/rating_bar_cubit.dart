import 'package:actividad_05/models/entity.dart';
import 'package:actividad_05/repositories/firebase_repository.dart';
import 'package:equatable/equatable.dart';
import 'package:bloc/bloc.dart';
part 'rating_bar_state.dart';

class RatingBarCubit extends Cubit<RatingBarState> {
  RatingBarCubit({this.entity, this.repository})
      : super(RatingBarInitialState());

  final Entity entity;
  final FirebaseRepository repository;

  String _path<T extends Entity>(id) => '$T\/$id'.toLowerCase();
  double _getRating(data) => data != null ? data['rating'].toDouble() : 0.0;

  listenRating<T extends Entity>() {
    repository.listen(_path<T>(entity.id), (data) {
      emit(RatingBarLoadingState());
      emit(RatingBarLoadedState(_getRating(data)));
    });
  }

  setRating<T extends Entity>(double rating) {
    repository.write(_path<T>(entity.id), {'rating': rating});
  }
}
