part of 'rating_bar_cubit.dart';

abstract class RatingBarState extends Equatable {
  const RatingBarState();

  @override
  List<Object> get props => [];
}

class RatingBarInitialState extends RatingBarState {}
class RatingBarLoadingState extends RatingBarState {}
class RatingBarLoadedState extends RatingBarState {
  const RatingBarLoadedState(this.rating);
  final double rating;
}
