import 'package:actividad_05/common/templates/details/widgets/rating_bar/cubit/rating_bar_cubit.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart' as RatingBarWidget;
import 'package:actividad_05/models/entity.dart';

class RatingBar<T extends Entity> extends StatelessWidget {
  const RatingBar({Key key}) : super(key: key);

  Widget build(BuildContext context) {
    final RatingBarCubit cubit = context.read<RatingBarCubit>();
    return BlocBuilder<RatingBarCubit, RatingBarState>(
        builder: (context, state) {

      if (state is RatingBarInitialState) cubit.listenRating<T>();

      if (state is RatingBarLoadingState)
        return Center(child: CircularProgressIndicator());

      if (state is RatingBarLoadedState)
        return RatingBarWidget.RatingBar.builder(
          initialRating: state.rating,
          minRating: 0.5,
          direction: Axis.horizontal,
          allowHalfRating: true,
          itemCount: 5,
          itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
          itemBuilder: (context, _) => Icon(
            Icons.star,
            color: Colors.amber,
          ),
          onRatingUpdate: (rating) => cubit.setRating<T>(rating),
        );
      else
        return SizedBox();
    });
  }
}
