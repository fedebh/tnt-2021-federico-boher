part of 'favorite_button_cubit.dart';

abstract class FavoriteButtonState extends Equatable {
  const FavoriteButtonState();

  @override
  List<Object> get props => [];
}

class FavoriteButtonInitialState extends FavoriteButtonState {}
class FavoriteButtonLoadingState extends FavoriteButtonState {}
class FavoriteButtonLoadedState extends FavoriteButtonState {
  const FavoriteButtonLoadedState(this.isFavorite);
  final bool isFavorite;
}
