import 'package:actividad_05/models/entity.dart';
import 'package:equatable/equatable.dart';
import 'package:actividad_05/repositories/favorites_repository.dart';
import 'package:bloc/bloc.dart';

part 'favorite_button_state.dart';

class FavoriteButtonCubit extends Cubit<FavoriteButtonState> {
  FavoriteButtonCubit({this.entity, this.repository}) : super(FavoriteButtonInitialState());

  final Entity entity;
  final FavoritesRepository repository;

  loadFavorite<T extends Entity>() async {
    bool isFavorite = await repository.fetchFavorite<T>(entity);
    emit(FavoriteButtonLoadedState(isFavorite));
  }

  toggleFavorite<T extends Entity>() async {
    emit(FavoriteButtonLoadingState());
    bool isFavorite = await repository.toggleFavorite<T>(entity);
    emit(FavoriteButtonLoadedState(isFavorite));
  }
}