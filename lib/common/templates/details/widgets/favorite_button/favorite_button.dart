import 'package:actividad_05/common/templates/details/widgets/favorite_button/cubit/favorite_button_cubit.dart';
import 'package:actividad_05/models/entity.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class FavoriteButton<T extends Entity> extends StatelessWidget {
  const FavoriteButton({Key key}) : super(key: key);

  Widget build(BuildContext context) {
    final FavoriteButtonCubit cubit = context.read<FavoriteButtonCubit>();

    return BlocBuilder<FavoriteButtonCubit, FavoriteButtonState>(
        builder: (context, state) {
      if (state is FavoriteButtonInitialState) cubit.loadFavorite<T>();

      if (state is FavoriteButtonLoadedState)
        return IconButton(
          onPressed: () => cubit.toggleFavorite<T>(),
          icon: state.isFavorite
              ? Icon(Icons.favorite)
              : Icon(Icons.favorite_outline),
          iconSize: 40.0,
          color: Colors.black,
        );
      else
        return IconButton(
            onPressed: () => print("Error"),
            icon: Icon(Icons.favorite_outline),
            iconSize: 40.0,
            color: Colors.grey);
    });
  }
}
