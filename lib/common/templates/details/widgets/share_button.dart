import 'package:actividad_05/utils/structs.dart';
import 'package:flutter/material.dart';

class ShareButton extends StatelessWidget {
  const ShareButton({Key key, this.action}) : super(key: key);

  final Pair<Function, List<dynamic>> action;

  Widget build(BuildContext context) {
    return IconButton(
      onPressed: () => action.item1(context, action.item2),
      icon: Icon(Icons.share),
      iconSize: 35.0,
      color: Colors.black,
    );
  }
}
