import 'package:actividad_05/common/templates/details/widgets/favorite_button/cubit/favorite_button_cubit.dart';
import 'package:actividad_05/common/templates/details/widgets/rating_bar/cubit/rating_bar_cubit.dart';
import 'package:actividad_05/common/templates/details/widgets/rating_bar/rating_bar.dart';
import 'package:actividad_05/repositories/firebase_repository.dart';
import 'package:actividad_05/utils/structs.dart';
import 'package:actividad_05/models/entity.dart';
import 'package:actividad_05/common/templates/details/widgets/favorite_button/favorite_button.dart';
import 'package:actividad_05/common/templates/details/widgets/share_button.dart';
import 'package:actividad_05/repositories/favorites_repository.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class DetailScreenTemplate<T extends Entity> extends StatelessWidget {
  const DetailScreenTemplate(
      {Key key,
      this.entity,
      this.header,
      this.title,
      this.description,
      this.share,
      this.relatedContentTitle,
      this.relatedContent,
      this.footer})
      : super(key: key);

  final Entity entity;
  final String title;
  final String description;
  final Pair<Function, List<dynamic>> share;
  final dynamic header;
  final String relatedContentTitle;
  final dynamic relatedContent;
  final dynamic footer;

  String setRelatedContentTitle() {
    if (relatedContentTitle == null)
      return 'RELATED CONTENT';
    else if (relatedContentTitle.isNotEmpty)
      return relatedContentTitle.toUpperCase();
    else
      return '';
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: ListView(
        children: <Widget>[
          header ?? SizedBox(),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              BlocProvider(
                  create: (context) => FavoriteButtonCubit(
                      repository: context.read<FavoritesRepository>(),
                      entity: entity),
                  child: FavoriteButton<T>()),
              share != null
                  ? ShareButton(
                      action: share,
                    )
                  : SizedBox()
            ],
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Text(
                title.toUpperCase() ?? '',
                style: TextStyle(
                  fontSize: 20.0,
                  fontWeight: FontWeight.bold,
                ),
                textAlign: TextAlign.center,
              ),
              SizedBox(height: 12.0),
              BlocProvider(
                  create: (context) => RatingBarCubit(
                      repository: context.read<FirebaseRepository>(),
                      entity: entity),
                  child: RatingBar<T>()),
              SizedBox(height: 15.0),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 15.0),
                height: 120.0,
                child: SingleChildScrollView(
                  child: Text(
                    description ?? '',
                    style: TextStyle(
                      color: Colors.black54,
                    ),
                  ),
                ),
              ),
            ],
          ),
          Text(
            setRelatedContentTitle(),
            style: TextStyle(
              fontSize: 20.0,
              fontWeight: FontWeight.bold,
            ),
            textAlign: TextAlign.center,
          ),
          relatedContent ?? SizedBox(),
          footer ?? SizedBox(),
        ],
      ),
    );
  }
}
