import 'package:actividad_05/models/serializable.dart';
import 'anime/anime.dart';
import 'comics/character.dart';
import 'comics/comic.dart';
import 'comics/creator.dart';
import 'comics/marvel_event.dart';
import 'comics/series.dart';

abstract class Entity implements Serializable {
  const Entity({this.id});
  final int id;

  static deserialize<T>(item) {
    switch (T) {
      case Character:
        return Character.fromJson(item);
      case Series:
        return Series.fromJson(item);
      case Comic:
        return Comic.fromJson(item);
      case MarvelEvent:
        return MarvelEvent.fromJson(item);
      case Creator:
        return Creator.fromJson(item);
      case Anime:
        return Anime.fromJson(item);
      default:
        throw Exception('Type not supported');
    }
  }
}