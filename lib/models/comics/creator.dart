import 'package:actividad_05/models/entity.dart';
import 'package:actividad_05/models/comics/thumbnail.dart';
import 'marvel_url.dart';

class Creator extends Entity {
  Creator({
    id,
    this.firstName,
    this.middleName,
    this.lastName,
    this.suffix,
    this.fullName,
    this.modified,
    this.thumbnail,
    this.resourceUri,
    this.urls,
  }) : super(id: id);

  String firstName;
  String middleName;
  String lastName;
  String suffix;
  String fullName;
  String modified;
  Thumbnail thumbnail;
  String resourceUri;
  List<MarvelUrl> urls;

  @override
  String toString() => 'creator';

  factory Creator.fromJson(Map<String, dynamic> json) => Creator(
        id: json['id'],
        firstName: json['firstName'],
        middleName: json['middleName'],
        lastName: json['lastName'],
        suffix: json['suffix'],
        fullName: json['fullName'],
        modified: json['modified'],
        thumbnail: Thumbnail.fromJson(json['thumbnail']),
        resourceUri: json['resourceURI'],
        urls: List<MarvelUrl>.from(
            json['urls'].map((x) => MarvelUrl.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        'id': id,
        'firstName': firstName,
        'middleName': middleName,
        'lastName': lastName,
        'suffix': suffix,
        'fullName': fullName,
        'modified': modified,
        'thumbnail': thumbnail.toJson(),
        'resourceURI': resourceUri,
        'urls': List<dynamic>.from(urls.map((x) => x.toJson())),
      };
}
