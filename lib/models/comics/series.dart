import 'package:actividad_05/models/entity.dart';
import 'package:actividad_05/utils/helpers.dart';
import 'package:actividad_05/models/comics/thumbnail.dart';
import 'marvel_url.dart';

class Series extends Entity {
  Series({
    id,
    this.title,
    this.description,
    this.resourceUri,
    this.urls,
    this.startYear,
    this.endYear,
    this.rating,
    this.type,
    this.modified,
    this.thumbnail,
  }) : super(id: id);

  String title;
  String description;
  String resourceUri;
  List<MarvelUrl> urls;
  int startYear;
  int endYear;
  String rating;
  SeriesType type;
  String modified;
  Thumbnail thumbnail;

  @override
  String toString() => 'series';

  factory Series.fromJson(Map<String, dynamic> json) => Series(
        id: json['id'],
        title: json['title'],
        description: json['description'] == null ? '' : json['description'],
        resourceUri: json['resourceURI'],
        urls: List<MarvelUrl>.from(
            json['urls'].map((x) => MarvelUrl.fromJson(x))),
        startYear: json['startYear'],
        endYear: json['endYear'],
        rating: json['rating'],
        type: serieTypeValues.map[json['type']],
        modified: json['modified'],
        thumbnail: Thumbnail.fromJson(json['thumbnail']),
      );

  Map<String, dynamic> toJson() => {
        'id': id,
        'title': title,
        'description': description == null ? '' : description,
        'resourceURI': resourceUri,
        'urls': List<dynamic>.from(urls.map((x) => x.toJson())),
        'startYear': startYear,
        'endYear': endYear,
        'rating': rating,
        'type': serieTypeValues.reverse[type],
        'modified': modified,
        'thumbnail': thumbnail.toJson(),
      };
}

enum SeriesType { ONE_SHOT, LIMITED, EMPTY }

final serieTypeValues = EnumValues({
  '': SeriesType.EMPTY,
  'limited': SeriesType.LIMITED,
  'one shot': SeriesType.ONE_SHOT
});
