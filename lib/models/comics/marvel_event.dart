import 'package:actividad_05/models/comics/thumbnail.dart';
import 'package:actividad_05/models/entity.dart';
import 'marvel_url.dart';

class MarvelEvent extends Entity {
  MarvelEvent({
    id,
    this.title,
    this.description,
    this.resourceUri,
    this.urls,
    this.modified,
    this.start,
    this.end,
    this.thumbnail,
  }) : super(id: id);

  String title;
  String description;
  String resourceUri;
  List<MarvelUrl> urls;
  String modified;
  DateTime start;
  DateTime end;
  Thumbnail thumbnail;

  @override
  String toString() => 'event';

  factory MarvelEvent.fromJson(Map<String, dynamic> json) => MarvelEvent(
        id: json['id'],
        title: json['title'],
        description: json['description'] == null ? '' : json['description'],
        resourceUri: json['resourceURI'],
        urls: List<MarvelUrl>.from(
            json['urls'].map((x) => MarvelUrl.fromJson(x))),
        modified: json['modified'],
        start: json['start'] == null ? null : DateTime.parse(json['start']),
        end: json['end'] == null ? null : DateTime.parse(json['end']),
        thumbnail: Thumbnail.fromJson(json['thumbnail']),
      );

  Map<String, dynamic> toJson() => {
        'id': id,
        'title': title,
        'description': description == null ? '' : description,
        'resourceURI': resourceUri,
        'urls': List<dynamic>.from(urls.map((x) => x.toJson())),
        'modified': modified,
        'start': start == null ? null : start.toIso8601String(),
        'end': end == null ? null : end.toIso8601String(),
        'thumbnail': thumbnail.toJson(),
      };
}
