import 'dart:convert';
import 'package:actividad_05/models/entity.dart';
import 'package:actividad_05/models/comics/thumbnail.dart';
import 'marvel_url.dart';

class Character extends Entity {
  Character(
      {id,
      this.name,
      this.description,
      this.modified,
      this.thumbnail,
      this.resourceUri,
      this.urls})
      : super(
          id: id,
        );

  String name;
  String description;
  String modified;
  Thumbnail thumbnail;
  String resourceUri;
  List<MarvelUrl> urls;

  @override
  String toString() => 'character';

  factory Character.fromRawJson(String str) =>
      Character.fromJson(json.decode(str));

  factory Character.fromJson(Map<String, dynamic> json) => Character(
      id: json['id'],
      name: json['name'],
      description: json['description'],
      modified: json['modified'],
      thumbnail: Thumbnail.fromJson(json['thumbnail']),
      resourceUri: json['resourceURI'],
      urls:
          List<MarvelUrl>.from(json['urls'].map((x) => MarvelUrl.fromJson(x))));

  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'description': description,
        'modified': modified,
        'thumbnail': thumbnail.toJson(),
        'resourceURI': resourceUri,
        'urls': List<dynamic>.from(urls.map((x) => x.toJson())),
      };

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'name': name,
      'description': description,
      'modified': modified,
      'resourceURI': resourceUri,
      'urls': urls,
    };
  }
}
