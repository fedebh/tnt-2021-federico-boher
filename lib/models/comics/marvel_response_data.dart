import 'dart:convert';
import 'package:actividad_05/models/entity.dart';
import 'package:actividad_05/models/serializable.dart';

/// **Container** object\
/// Displays pagination information and an array of the results returned by this call.
/// https://developer.marvel.com/documentation/apiresults
class MarvelResponseData<T> implements Serializable {
  MarvelResponseData({
    this.offset,
    this.limit,
    this.total,
    this.count,
    this.results,
  });

  int offset;
  int limit;
  int total;
  int count;
  List<T> results;

  factory MarvelResponseData.fromRawJson(String str) =>
      MarvelResponseData.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory MarvelResponseData.fromJson(Map<String, dynamic> json) =>
      MarvelResponseData(
        offset: json['offset'],
        limit: json['limit'],
        total: json['total'],
        count: json['count'],
        results: List<T>.from(json['results'].map((item) {
          return Entity.deserialize<T>(item);
        })),
      );

  Map<String, dynamic> toJson() => {
        'offset': offset,
        'limit': limit,
        'total': total,
        'count': count,
        'results': List<dynamic>.from(results.map((item) {
          return Entity.deserialize<T>(item);
        })),
      };
}
