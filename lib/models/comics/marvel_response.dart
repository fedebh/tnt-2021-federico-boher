import 'dart:convert';
import 'package:actividad_05/models/serializable.dart';
import 'marvel_response_data.dart';

/// Results returned by the API endpoints have the same general format, no matter which entity type the endpoint returns.
/// Every successful call will return a **wrapper** object, which contains metadata about the call and a **container** object,
/// which displays pagination information and an array of the results returned by this call.
/// This pattern is consistent even if you are requesting a single object.
/// https://developer.marvel.com/documentation/apiresults
class MarvelResponse<T> implements Serializable {
  MarvelResponse({
    this.code,
    this.status,
    this.copyright,
    this.attributionText,
    this.attributionHtml,
    this.etag,
    this.data,
  });

  dynamic code;
  String status;
  String copyright;
  String attributionText;
  String attributionHtml;
  String etag;
  MarvelResponseData<T> data;

  factory MarvelResponse.fromRawJson(String str) =>
      MarvelResponse<T>.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory MarvelResponse.fromJson(Map<String, dynamic> json) => MarvelResponse(
        code: json['code'],
        status: json['status'],
        copyright: json['copyright'],
        attributionText: json['attributionText'],
        attributionHtml: json['attributionHTML'],
        etag: json['etag'],
        data: MarvelResponseData<T>.fromJson(json['data']),
      );

  factory MarvelResponse.connect(Map<String, dynamic> json) => MarvelResponse(
        code: json['code'],
        status: json['status'],
      );

  bool canConnect(MarvelResponse response) {
    return response.code is int;
  }

  Map<String, dynamic> toJson() => {
        'code': code,
        'status': status,
        'copyright': copyright,
        'attributionText': attributionText,
        'attributionHTML': attributionHtml,
        'etag': etag,
        'data': data.toJson(),
      };
}
