import 'package:actividad_05/models/entity.dart';
import 'package:actividad_05/utils/helpers.dart';
import 'package:actividad_05/models/comics/thumbnail.dart';
import 'marvel_url.dart';

class Comic extends Entity {
  Comic({
    id,
    this.digitalId,
    this.title,
    this.issueNumber,
    this.variantDescription,
    this.description,
    this.modified,
    this.isbn,
    this.upc,
    this.diamondCode,
    this.ean,
    this.issn,
    this.format,
    this.pageCount,
    this.resourceUri,
    this.urls,
    this.thumbnail,
    this.images,
  }) : super(id: id);

  int digitalId;
  String title;
  int issueNumber;
  String variantDescription;
  String description;
  String modified;
  String isbn;
  String upc;
  String diamondCode;
  String ean;
  String issn;
  Format format;
  int pageCount;
  String resourceUri;
  List<MarvelUrl> urls;
  Thumbnail thumbnail;
  List<Thumbnail> images;

  @override
  String toString() => 'comic';

  factory Comic.fromJson(Map<String, dynamic> json) => Comic(
        id: json['id'],
        digitalId: json['digitalId'],
        title: json['title'],
        issueNumber: json['issueNumber'],
        variantDescription: json['variantDescription'],
        description: json['description'] == null ? '' : json['description'],
        modified: json['modified'],
        isbn: json['isbn'],
        upc: json['upc'],
        diamondCode: json['diamondCode'],
        ean: json['ean'],
        issn: json['issn'],
        format: formatValues.map[json['format']],
        pageCount: json['pageCount'],
        resourceUri: json['resourceURI'],
        urls: List<MarvelUrl>.from(
            json['urls'].map((x) => MarvelUrl.fromJson(x))),
        thumbnail: Thumbnail.fromJson(json['thumbnail']),
        images: List<Thumbnail>.from(
            json['images'].map((x) => Thumbnail.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        'id': id,
        'digitalId': digitalId,
        'title': title,
        'issueNumber': issueNumber,
        'variantDescription': variantDescription,
        'description': description == null ? '' : description,
        'modified': modified,
        'isbn': isbn,
        'upc': upc,
        'diamondCode': diamondCode,
        'ean': ean,
        'issn': issn,
        'format': formatValues.reverse[format],
        'pageCount': pageCount,
        'resourceURI': resourceUri,
        'urls': List<dynamic>.from(urls.map((x) => x.toJson())),
        'thumbnail': thumbnail.toJson(),
        'images': List<dynamic>.from(images.map((x) => x.toJson())),
      };
}

enum Format { COMIC, INFINITE_COMIC, TRADE_PAPERBACK, HARDCOVER }

final formatValues = EnumValues({
  'Comic': Format.COMIC,
  'Hardcover': Format.HARDCOVER,
  'Infinite Comic': Format.INFINITE_COMIC,
  'Trade Paperback': Format.TRADE_PAPERBACK
});
