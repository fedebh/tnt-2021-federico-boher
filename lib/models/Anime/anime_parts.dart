part of 'anime.dart';

class AnimeTitle implements Serializable {
  AnimeTitle({this.userPreferred});
  String userPreferred;

  factory AnimeTitle.fromJson(Map<String, dynamic> json) => AnimeTitle(
      userPreferred:
          json['userPreferred'] == null ? null : json['userPreferred']);

  Map<String, dynamic> toJson() => {
        'userPreferred': userPreferred,
      };
}

class AnimeNextAiringEpisode implements Serializable {
  AnimeNextAiringEpisode({this.episode, this.airingAt});
  int episode;
  int airingAt;

  factory AnimeNextAiringEpisode.fromJson(Map<String, dynamic> json) =>
      AnimeNextAiringEpisode(
          episode: json['episode'] == null ? null : json['episode'],
          airingAt: json['airingAt'] == null ? null : json['airingAt']);

  Map<String, dynamic> toJson() => {'episode': episode, 'airingAt': airingAt};

  String getDateMMddyyyy() =>
      "$episode: " +
      DateFormat("MM/dd/yyyy")
          .format(DateTime.fromMillisecondsSinceEpoch(airingAt * 1000));
}

class AnimeStartingDate implements Serializable {
  AnimeStartingDate({this.day, this.month, this.year});
  int day;
  int month;
  int year;

  factory AnimeStartingDate.fromJson(Map<String, dynamic> json) =>
      AnimeStartingDate(
          day: json['day'] == null ? null : json['day'],
          month: json['month'] == null ? null : json['month'],
          year: json['year'] == null ? null : json['year']);

  Map<String, dynamic> toJson() => {'day': day, 'month': month, 'year': year};

  String getDateMMddyyyy() => "$month/$day/$year";
}

class AnimeExternalLink implements Serializable {
  AnimeExternalLink({this.site, this.url});
  String site;
  String url;

  factory AnimeExternalLink.fromJson(Map<String, dynamic> json) =>
      AnimeExternalLink(
          site: json['site'] == null ? null : json['site'],
          url: json['url'] == null ? null : json['url']);

  Map<String, dynamic> toJson() => {
        'site': site,
        'url': url,
      };
}

class AnimeCoverImage implements Serializable {
  AnimeCoverImage({this.large, this.extraLarge});
  String large;
  String extraLarge;

  factory AnimeCoverImage.fromJson(Map<String, dynamic> json) =>
      AnimeCoverImage(
          large: json['large'] == null ? null : json['large'],
          extraLarge: json['extraLarge'] == null ? null : json['extraLarge']);

  Map<String, dynamic> toJson() => {
        'large': large,
        'extraLarge': extraLarge,
      };
}

class AnimeTrailer implements Serializable {
  AnimeTrailer({this.id});
  String id;

  factory AnimeTrailer.fromJson(Map<String, dynamic> json) =>
      AnimeTrailer(id: json['id'] == null ? null : json['id']);

  Map<String, dynamic> toJson() => {
        'id': id,
      };
}