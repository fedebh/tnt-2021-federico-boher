import 'package:actividad_05/models/serializable.dart';
import 'package:actividad_05/models/entity.dart';
import 'package:intl/intl.dart';
part 'anime_parts.dart';

class Anime extends Entity {
  Anime(
      {id,
      this.title,
      this.description,
      this.nextAiringEpisode,
      this.duration,
      this.status,
      this.startDate,
      this.season,
      this.seasonYear,
      this.averageScore,
      this.meanScore,
      this.popularity,
      this.favourites,
      this.coverImage,
      this.format,
      this.externalLinks,
      this.trailer})
      : super(id: id);

  AnimeTitle title;
  String description;
  AnimeNextAiringEpisode nextAiringEpisode;
  int duration;
  String status;
  AnimeStartingDate startDate;
  String season;
  int seasonYear;
  int averageScore;
  int meanScore;
  int popularity;
  int favourites;
  AnimeCoverImage coverImage;
  String format;
  List<AnimeExternalLink> externalLinks;
  AnimeTrailer trailer;

  factory Anime.fromJson(Map<dynamic, dynamic> json) => Anime(
      id: json['id'],
      title: json['title'] == null ? null : AnimeTitle.fromJson(json['title']),
      description: json['description'],
      nextAiringEpisode: json['nextAiringEpisode'] == null
          ? null
          : AnimeNextAiringEpisode.fromJson(json['nextAiringEpisode']),
      format: json['format'],
      duration: json['duration'],
      status: json['status'],
      startDate: json['startDate'] == null
          ? null
          : AnimeStartingDate.fromJson(json['startDate']),
      season: json['season'],
      seasonYear: json['seasonYear'],
      averageScore: json['averageScore'],
      meanScore: json['meanScore'],
      popularity: json['popularity'],
      favourites: json['favourites'],
      coverImage: json['coverImage'] == null
          ? null
          : AnimeCoverImage.fromJson(json['coverImage']),
      trailer: json['trailer'] == null
          ? null
          : AnimeTrailer.fromJson(json['trailer']),
      externalLinks: json['externalLinks'] == null
          ? null
          : json['externalLinks']
              .map<AnimeExternalLink>(
                  (item) => AnimeExternalLink.fromJson(item))
              .toList());

  factory Anime.cardFromJson(Map<String, dynamic> json) => Anime(
      id: json['id'],
      title: AnimeTitle.fromJson(json['title']),
      coverImage: AnimeCoverImage.fromJson(json['coverImage']));

  Map<String, dynamic> toJson() => {
        'id': id,
        'title': title == null ? null : title.toJson(),
        'description': description,
        'nextAiringEpisode': nextAiringEpisode == null ? null : nextAiringEpisode.toJson(),
        'duration': duration,
        'status': status,
        'startDate': startDate.toJson(),
        'season': season,
        'averageScore': averageScore,
        'meanScore': meanScore,
        'popularity': popularity,
        'favourites': favourites,
        'coverImage': coverImage == null ? null : coverImage.toJson(),
        'format': format,
        'trailer': trailer == null ? null : trailer.toJson(),
        'externalLinks':
            List<dynamic>.from(externalLinks.map((link) => link.toJson())),
      };
}