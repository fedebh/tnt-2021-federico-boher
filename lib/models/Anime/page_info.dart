import 'package:actividad_05/models/serializable.dart';

class AnimePageInfo implements Serializable {
  AnimePageInfo(
      {this.total,
      this.perPage,
      this.currentPage,
      this.lastPage,
      this.hasNextPage});
  int total;
  int perPage;
  int currentPage;
  int lastPage;
  bool hasNextPage;

  factory AnimePageInfo.fromJson(Map<String, dynamic> json) =>
      AnimePageInfo(
        total: json['total'] == null ? null : json['total'],
        perPage: json['perPage'] == null ? null : json['perPage'],
        currentPage: json['currentPage'] == null ? null : json['currentPage'],
        lastPage: json['lastPage'] == null ? null : json['lastPage'],
        hasNextPage: json['hasNextPage'] == null ? null : json['hasNextPage'],
        );

  Map<String, dynamic> toJson() => {
        'total': total,
        'perPage': perPage,
        'currentPage': currentPage,
        'lastPage': lastPage,
        'hasNextPage': hasNextPage,
      };
}
