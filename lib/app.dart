import 'package:actividad_05/repositories/favorites_repository.dart';
import 'package:actividad_05/repositories/firebase_repository.dart';
import 'package:actividad_05/routes/routes.dart';
import 'package:actividad_05/services/graphql/anime_gql_service/anime_gql_service.dart';
import 'package:actividad_05/repositories/marvel_repository.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import 'package:flutter/material.dart';

// ignore: implementation_imports
import 'package:flutter_bloc/src/repository_provider.dart';

const APP_TITLE = 'My Comics&Anime';

class App extends StatelessWidget {
  const App(this.isInternetAvailable);
  final bool isInternetAvailable;

  /// Providers para toda la aplicacion
  List<RepositoryProviderSingleChildWidget> initProviders() {
    return [
      RepositoryProvider<FavoritesRepository>(
          create: (context) => FavoritesRepository()),
      RepositoryProvider<FirebaseRepository>(
          create: (context) => FirebaseRepository()),
      RepositoryProvider<MarvelRepository>(
          create: (context) => MarvelRepository()),
      RepositoryProvider<AnimeGQLService>(
          create: (context) => AnimeGQLService()),
    ];
  }

  @override
  Widget build(BuildContext context) {
    if (!isInternetAvailable) {
      return GetMaterialApp(
        title: APP_TITLE,
        theme: ThemeData(primaryColor: Colors.red),
        initialRoute: Routes.OFFLINE,
        getPages: pages,
      );
    }

    FavoritesRepository.getInstance();
    return MultiRepositoryProvider(
        providers: initProviders(),
        child: GetMaterialApp(
          title: APP_TITLE,
          theme: ThemeData(primaryColor: Colors.red),
          initialRoute: Routes.ROOT,
          getPages: pages,
        ));
  }
}
