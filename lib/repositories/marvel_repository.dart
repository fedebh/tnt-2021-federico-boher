import 'dart:convert';
import 'package:actividad_05/models/models.dart';
import 'package:convert/convert.dart';
import 'package:crypto/crypto.dart' as crypto;
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:http/http.dart' as http;

/// https://developer.marvel.com/account
class MarvelAPIKey {
  static final String public = dotenv.env['MARVEL_API_PUBLIC_KEY'];
  static final String private = dotenv.env['MARVEL_API_PRIVATE_KEY'];
}

/// https://developer.marvel.com/docs
class MarvelRepository {
  static const String MARVEL_API_BASE = 'gateway.marvel.com';
  static const String MARVEL_API_VERSION = '/v1/public';
  static const String MARVEL_ATTRIBUTION =
      'Data provided by Marvel. © 2014 Marvel';

  String publicKey = MarvelAPIKey.public;
  String privateKey = MarvelAPIKey.private;

  String _generateMd5(String data) {
    var content = new Utf8Encoder().convert(data);
    var digest = crypto.md5.convert(content);
    return hex.encode(digest.bytes);
  }

  Uri _buildUri(String path, {Map<String, String> queryParams}) {
    String ts = DateTime.now().millisecondsSinceEpoch.toString();
    Map<String, String> baseQryParams = {
      'ts': ts,
      'apikey': publicKey,
      'hash': _generateMd5(ts + privateKey + publicKey)
    };
    if (queryParams != null) {
      baseQryParams.addAll(queryParams);
    }
    Uri uri =
        Uri.https(MARVEL_API_BASE, MARVEL_API_VERSION + path, baseQryParams);
    return uri;
  }

  Map<dynamic, String> _names = {
    Character: 'characters',
    Comic: 'comics',
    Creator: 'creators',
    MarvelEvent: 'events',
    Series: 'series'
  };

  Future<MarvelResponse<T>> getAll<T extends Entity>(
          {Map<String, String> queryParams: const {'orderBy': '-modified'}}) =>
      http
          .get(_buildUri('/' + _names[T], queryParams: queryParams))
          .then((response) => MarvelResponse<T>.fromRawJson(response.body));

  Future<MarvelResponse<TRelated>>
      getRelated<TEntity extends Entity, TRelated extends Entity>(int id,
              {Map<String, String> queryParams: const {
                'orderBy': '-modified'
              }}) =>
          http
              .get(_buildUri('/' + _names[TEntity] + '/$id/' + _names[TRelated],
                  queryParams: queryParams))
              .then((response) =>
                  MarvelResponse<TRelated>.fromRawJson(response.body));
}
