import 'package:firebase_database/firebase_database.dart';

class FirebaseRepository {
  static DatabaseReference _ref = FirebaseDatabase.instance.ref();

  listen(String path, Function callback) {
    _ref = FirebaseDatabase.instance.ref(path);
    return _ref.onValue
        .listen((DatabaseEvent event) => callback(event.snapshot.value));
  }

  write(String path, Map<dynamic, dynamic> data) async {
    _ref = FirebaseDatabase.instance.ref(path);
    await _ref.set(data);
  }
}
