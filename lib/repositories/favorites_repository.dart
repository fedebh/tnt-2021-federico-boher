import 'dart:convert';
import 'package:actividad_05/models/models.dart';
import 'package:shared_preferences/shared_preferences.dart';

/// Inicializar con getInstance()
class FavoritesRepository {
  static SharedPreferences _sharedPreferences;
  static getInstance() async => _sharedPreferences == null
      ? _sharedPreferences = await SharedPreferences.getInstance()
      : _sharedPreferences;

  static String _getKey<T extends Entity>() => '$T'.toLowerCase();

  Future<bool> toggleFavorite<T extends Entity>(T entity) async {
    try {
      String key = _getKey<T>();
      String favs = _sharedPreferences.getString(key) ?? '{}';
      String id = entity.id.toString();
      Map<String, dynamic> favMap = json.decode(favs);
      bool isStored = favMap.containsKey(id);

      if (isStored) {
        favMap.remove(id);
        print('($T)-Removed -> $favMap');
      } else {
        favMap.putIfAbsent(id, () => entity.toJson());
        print('($T)-Added -> $favMap');
      }

      await _sharedPreferences.setString(key, json.encode(favMap));

      return !isStored;
    } catch (e) {
      print('Error-toggle: $e');
    }

    return false;
  }

  Future<bool> fetchFavorite<T extends Entity>(T entity) async {
    try {
      String favs = _sharedPreferences.getString(_getKey<T>()) ?? '{}';
      Map<String, dynamic> favMap = json.decode(favs);
      return favMap.containsKey(entity.id.toString());
    } catch (e) {
      print('Error-get: $e');
    }
    return false;
  }

  Future<List<T>> fetchFavorites<T extends Entity>() async {
    try {
      String favs = _sharedPreferences.getString(_getKey<T>()) ?? '{}';
      Map<String, dynamic> favMap = json.decode(favs);
      List<T> entities = [];
      favMap.forEach((key, value) {
        entities.add(Entity.deserialize<T>(value));
      });
      return entities;
    } catch (e) {}
    return null;
  }
}
