part of 'favorites_bloc.dart';

abstract class FavoritesState extends Equatable {
  const FavoritesState();

  @override
  List<Object> get props => [];
}

class FavoritesLoadingState extends FavoritesState {}

class FavoritesEmptyState extends FavoritesState {
  const FavoritesEmptyState(this.text);
  final String text;
}

class FavoritesAllLoadedState extends FavoritesState {
  const FavoritesAllLoadedState(this.map);
  final Map<dynamic, List<dynamic>> map;
}

class FavoritesLoadedState<T> extends FavoritesState {
  const FavoritesLoadedState(this.list);
  final List<T> list;
}
