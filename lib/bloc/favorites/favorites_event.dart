part of 'favorites_bloc.dart';

abstract class FavoritesEvent extends Equatable {
  const FavoritesEvent();

  @override
  List<Object> get props => null;
}

class FavoritesLoad<T extends Entity> extends FavoritesEvent {
  const FavoritesLoad();
}

class FavoritesWait extends FavoritesEvent {
  const FavoritesWait();
}

class FavoritesLoadAll extends FavoritesEvent {
  const FavoritesLoadAll();
}