import 'package:actividad_05/utils/helpers.dart';
import 'package:actividad_05/models/models.dart';
import 'package:actividad_05/repositories/favorites_repository.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

part 'favorites_event.dart';
part 'favorites_state.dart';

class FavoritesBloc extends Bloc<FavoritesEvent, FavoritesState> {
  FavoritesBloc({this.repository}) : super(FavoritesLoadingState());
  final FavoritesRepository repository;

  List<Character> characters;
  List<Series> series;
  List<Comic> comics;
  List<MarvelEvent> marvelEvents;
  List<Creator> creators;
  List<Anime> anime;

  @override
  Stream<FavoritesState> mapEventToState(FavoritesEvent event) async* {
    if (event is FavoritesWait) {
      yield* _mapWaitingToStates(event);
    } else if (event is FavoritesLoadAll) {
      yield* _mapLoadAllToStates(event);
    } else if (event is FavoritesLoad<Character>) {
      yield* _mapLoadToStates<Character>(event);
    } else if (event is FavoritesLoad<Series>) {
      yield* _mapLoadToStates<Series>(event);
    } else if (event is FavoritesLoad<Comic>) {
      yield* _mapLoadToStates<Comic>(event);
    } else if (event is FavoritesLoad<MarvelEvent>) {
      yield* _mapLoadToStates<MarvelEvent>(event);
    } else if (event is FavoritesLoad<Creator>) {
      yield* _mapLoadToStates<Creator>(event);
    } else if (event is FavoritesLoad<Anime>) {
      yield* _mapLoadToStates<Anime>(event);
    }
  }

  Stream<FavoritesState> _mapWaitingToStates(FavoritesWait event) async* {
    yield FavoritesLoadingState();
  }

  Stream<FavoritesState> _mapLoadAllToStates(FavoritesLoadAll event) async* {
    characters = await repository.fetchFavorites<Character>();
    series = await repository.fetchFavorites<Series>();
    comics = await repository.fetchFavorites<Comic>();
    marvelEvents = await repository.fetchFavorites<MarvelEvent>();
    creators = await repository.fetchFavorites<Creator>();
    anime = await repository.fetchFavorites<Anime>();

    Map<dynamic, List<dynamic>> mapList = {
      Character: characters,
      Series: series,
      Comic: comics,
      MarvelEvent: marvelEvents,
      Creator: creators,
      Anime: anime,
    };

    if (Helpers.isMapListEmpty(mapList))
      yield FavoritesEmptyState('Your favorite list is empty.');
    else
      yield FavoritesAllLoadedState(mapList);
  }

  Stream<FavoritesState> _mapLoadToStates<T extends Entity>(
      FavoritesLoad<T> event) async* {
    List<T> list = await repository.fetchFavorites<T>();

    if (list.isEmpty)
      yield FavoritesEmptyState('Your favorite list is empty.');
    else
      yield FavoritesLoadedState<T>(list);
    
  }
}
