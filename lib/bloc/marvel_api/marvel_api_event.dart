part of 'marvel_api_bloc.dart';

abstract class MarvelAPIEvent extends Equatable {
  const MarvelAPIEvent();

  @override
  List<Object> get props => null;
}

class MarvelAPIGetInitializedData extends MarvelAPIEvent {
  const MarvelAPIGetInitializedData(this.data);
  final dynamic data;

  @override
  List<Object> get props => [data];
}

class MarvelAPIFetchNextPage<T extends Entity> extends MarvelAPIEvent {
  const MarvelAPIFetchNextPage({this.data});
  final dynamic data;
}
