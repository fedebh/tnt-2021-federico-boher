part of 'marvel_api_bloc.dart';

abstract class MarvelAPIState extends Equatable {
  const MarvelAPIState();

  @override
  List<Object> get props => null;
}

class MarvelAPILoadingState extends MarvelAPIState {
  const MarvelAPILoadingState();
}

class MarvelAPILoadSuccessState extends MarvelAPIState {
  const MarvelAPILoadSuccessState(this.data);
  final dynamic data;
}

class MarvelAPILoadFailState extends MarvelAPIState {}
class MarvelAPIEmptyState extends MarvelAPIState {}
class MarvelAPIEndPageReachedState extends MarvelAPIState {}