import 'package:actividad_05/models/comics.dart';
import 'package:actividad_05/models/entity.dart';
import 'package:actividad_05/repositories/marvel_repository.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
part 'marvel_api_event.dart';
part 'marvel_api_state.dart';

abstract class MarvelAPIBloc extends Bloc<MarvelAPIEvent, MarvelAPIState> {
  MarvelAPIBloc({this.repository}) : super(MarvelAPILoadingState());
  final MarvelRepository repository;

  final int maxPageOffset = 20;
  int pageOffset = 20;
  int previousOffset = 0;
  int currentOffset = 20;
  bool isEndReached = false;

  Map<String, String> toNextPage() {
    previousOffset = currentOffset;
    currentOffset += pageOffset;
    return {'orderBy': '-modified', 'offset': currentOffset.toString()};
  }

  bool checkIfEndReached(MarvelResponse marvelResponse) {
    return marvelResponse.data.count == 0;
  }

  setPageOffset(MarvelResponse marvelResponse) =>
      marvelResponse.data.count < maxPageOffset
          ? pageOffset += marvelResponse.data.count
          : pageOffset = maxPageOffset;

  MarvelResponse<Character> characters;
  MarvelResponse<Series> series;
  MarvelResponse<Comic> comics;
  MarvelResponse<MarvelEvent> events;
  MarvelResponse<Creator> creators;

  static bool areEmptyResponses(List<MarvelResponse> responses) {
    bool isEmpty = true;
    responses.forEach((response) {
      if (response != null && response.data.count > 0) {
        isEmpty = false;
        return;
      }
    });
    return isEmpty;
  }

  Future<bool> canConnect() async {
    MarvelResponse<Character> response =
        await repository.getAll<Character>(queryParams: {'limit': '1'});
    return response.canConnect(response);
  }
}
