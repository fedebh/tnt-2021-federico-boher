part of 'graphql_bloc.dart';

abstract class GraphQLEvent extends Equatable {
  const GraphQLEvent();

  @override
  List<Object> get props => null;
}

class GraphQLGetInitializedData extends GraphQLEvent {
  const GraphQLGetInitializedData(this.data);

  final dynamic data;
  
  @override
  List<Object> get props => [data];
}

class GraphQLFetchData extends GraphQLEvent {
  final String query;
  final Map<String, dynamic> variables;

  GraphQLFetchData(this.query, {this.variables});

  @override
  List<Object> get props => [query, variables];
}

class GraphQLFetchPage extends GraphQLEvent {
  final String type;
  final String query;
  final Map<String, dynamic> variables;

  GraphQLFetchPage(this.query, {this.type, this.variables});

  @override
  List<Object> get props => [type, query, variables];
}