part of 'graphql_bloc.dart';

abstract class GraphQLState extends Equatable {
  GraphQLState();

  @override
  List<Object> get props => null;
}

class GraphQLLoadingState extends GraphQLState {}

class GraphQLLoadSuccessState extends GraphQLState {
  final dynamic data;

  GraphQLLoadSuccessState(this.data) : super();

  @override
  List<Object> get props => data;
}

class GraphQLLoadFailState extends GraphQLState {
  final dynamic error;

  GraphQLLoadFailState(this.error) : super();

  @override
  List<Object> get props => error;
}

class GraphQLEndPageReachedState extends GraphQLState {}