import 'package:equatable/equatable.dart';
import 'package:actividad_05/services/graphql/graphql_service.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
part 'graphql_event.dart';
part 'graphql_state.dart';

class GraphQLBloc extends Bloc<GraphQLEvent, GraphQLState> {
  GraphQLBloc({this.service}) : super(GraphQLLoadingState());
  final GraphQLService service;

  final int perPage = 20;
  int _currentPage = 1;
  List<dynamic> list = [];

  setCurrentPage(page) {
    _currentPage = page;
  }

  int toNextPage() => ++_currentPage;
  int getNextPage() => _currentPage + 1;
}
