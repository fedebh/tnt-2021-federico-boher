part of 'anime_gql_query.dart';

const String ANIME_HOME_QUERY = '''
query (\$season: MediaSeason, \$seasonYear: Int, \$nextSeason: MediaSeason) {
  trending: Page(page: 1, perPage: 20) {
    media(sort: TRENDING_DESC, type: ANIME, isAdult: false) {
      $_ANIME_COMMON_STRUCTURE_FRAGMENT
    }
  }
  season: Page(page: 1, perPage: 20) {
    media(season: \$season, seasonYear: \$seasonYear, sort: POPULARITY_DESC, type: ANIME, isAdult: false) {
      $_ANIME_COMMON_STRUCTURE_FRAGMENT
    }
  }
  nextSeason: Page(page: 1, perPage: 20) {
    media(season: \$nextSeason, seasonYear: \$seasonYear, sort: POPULARITY_DESC, type: ANIME, isAdult: false) {
      $_ANIME_COMMON_STRUCTURE_FRAGMENT
    }
  }
  popular: Page(page: 1, perPage: 20) {
    media(sort: POPULARITY_DESC, type: ANIME, isAdult: false) {
      $_ANIME_COMMON_STRUCTURE_FRAGMENT
    }
  }
  top: Page(page: 1, perPage: 20) {
    media(sort: SCORE_DESC, type: ANIME, isAdult: false) {
      $_ANIME_COMMON_STRUCTURE_FRAGMENT
    }
  }
}
''';
