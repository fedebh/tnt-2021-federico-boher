part of 'anime_gql_query.dart';

const Map<String, String> ANIME_LIST_QUERYMAP = {
  'trending': '''
query (\$page: Int, \$perPage: Int) {
  trending: Page(page: \$page, perPage: \$perPage) {
    $_ANIME_PAGE_INFO
    media(sort: TRENDING_DESC, type: ANIME, isAdult: false) {
      $_ANIME_COMMON_STRUCTURE_FRAGMENT
    }
  }
}
''',
  'season': '''
query (\$page: Int, \$perPage: Int, \$season: MediaSeason, \$seasonYear: Int) {
  season: Page(page: \$page, perPage: \$perPage) {
    $_ANIME_PAGE_INFO
    media(season: \$season, seasonYear: \$seasonYear, sort: POPULARITY_DESC, type: ANIME, isAdult: false) {
      $_ANIME_COMMON_STRUCTURE_FRAGMENT
    }
  }
}
''',
  'nextSeason': '''
query (\$page: Int, \$perPage: Int, \$season: MediaSeason, \$seasonYear: Int) {
  nextSeason: Page(page: \$page, perPage: \$perPage) {
    $_ANIME_PAGE_INFO
    media(season: \$season, seasonYear: \$seasonYear, sort: POPULARITY_DESC, type: ANIME, isAdult: false) {
      $_ANIME_COMMON_STRUCTURE_FRAGMENT
    }
  }
}
''',
  'popular': '''
query (\$page: Int, \$perPage: Int) {
  popular: Page(page: \$page, perPage: \$perPage) {
    $_ANIME_PAGE_INFO
    media(sort: POPULARITY_DESC, type: ANIME, isAdult: false) {
      $_ANIME_COMMON_STRUCTURE_FRAGMENT
    }
  }
}
''',
  'top': '''
query media(\$page: Int, \$perPage: Int) {
  top: Page(page: \$page, perPage: \$perPage) {
    $_ANIME_PAGE_INFO
    media(sort: SCORE_DESC, type: ANIME, isAdult: false) {
      $_ANIME_COMMON_STRUCTURE_FRAGMENT
    }
  }
}
'''
};
