part of 'anime_gql_query.dart';

const String ANIME_PAGEINFO_SEARCH_BY_TEXT_QUERY = """
query (\$searchText: String, \$page: Int, \$perPage: Int) {
  $ANIME_SEARCH_TYPE: Page(page: \$page, perPage: \$perPage) {
    $_ANIME_PAGE_INFO
    media(sort: TRENDING_DESC, type: ANIME, isAdult: false, search: \$searchText) {
      title {
        userPreferred
      }   
    }
  }
}
""";

const String ANIME_SEARCH_BY_TEXT_QUERY = """
query (\$searchText: String, \$page: Int, \$perPage: Int) {
  $ANIME_SEARCH_TYPE: Page(page: \$page, perPage: \$perPage) {
    $_ANIME_PAGE_INFO
    media(sort: TRENDING_DESC, type: ANIME, isAdult: false, search: \$searchText) {
      id
      title {
        userPreferred
      }
      coverImage {
        extraLarge
        large
        color
      }
      startDate {
        year
        month
        day
      }
      endDate {
        year
        month
        day
      }
      bannerImage
      season
      status(version: 2)
      episodes
      # genres
      averageScore
      popularity
      studios(isMain: true) {
        edges {
          isMain
          node {
            id
            name
          }
        }
      }     
    }
  }
}
""";
