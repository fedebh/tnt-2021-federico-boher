part 'home_query.dart';
part 'details_query.dart';
part 'lists_query.dart';
part 'search_query.dart';

const String ANIME_MEDIA_TYPE = 'media';
const String ANIME_SEARCH_TYPE = 'animeSearch';
const String ANIME_PAGEINFO_TYPE = 'pageInfo';

const String _ANIME_COMMON_STRUCTURE_FRAGMENT = r'''
id
title {
  userPreferred
}
coverImage {
  large
}
''';

const String _ANIME_PAGE_INFO = r'''
pageInfo {
  total
  perPage
  currentPage
  lastPage
  hasNextPage
}''';