import 'package:actividad_05/services/graphql/graphql_service.dart';

/// https://anilist.co/graphiql
class AnimeGQLService extends GraphQLService {
  AnimeGQLService() : super('https://graphql.anilist.co');
}