import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:graphql/client.dart';

abstract class GraphQLService {
  GraphQLClient _client;

  GraphQLService(uri) {
    HttpLink link = HttpLink(uri);
    
    _client = GraphQLClient(link: link, cache: GraphQLCache(store: InMemoryStore()));
  }

  Future<QueryResult> performQuery(String query,
      {Map<String, dynamic> variables}) async {
    QueryOptions options =
        QueryOptions(document: gql(query), variables: variables);

    final result = await _client.query(options);

    return result;
  }

  Future<QueryResult> performMutation(String query,
      {Map<String, dynamic> variables}) async {
    MutationOptions options =
        MutationOptions(document: gql(query), variables: variables);

    final result = await _client.mutate(options);
    return result;
  }
}