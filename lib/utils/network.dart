import 'dart:io';

class Network {
  static Future<bool> checkInternetConnection({String address}) async {
    if (address == null) address = 'example.com';

    try {
      final result = await InternetAddress.lookup(address);
      return result.isNotEmpty && result[0].rawAddress.isNotEmpty;
    } on SocketException catch (_) {
      return false;
    }
  }
}
