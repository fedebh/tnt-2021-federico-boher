import 'dart:async';

class Helpers {
  /// Chequea si un mapa de listas tiene todas las listas vacias.
  static isMapListEmpty(Map<dynamic, List<dynamic>> mapList) {
    bool isEmpty = true;
    mapList.forEach((key, value) {
      if (value.isNotEmpty) {
        isEmpty = false;
        return;
      }
    });
    return isEmpty;
  }
}

class EnumValues<T> {
  Map<String, T> map;
  Map<T, String> reverseMap;

  EnumValues(this.map);

  Map<T, String> get reverse {
    if (reverseMap == null) reverseMap = map.map((k, v) => new MapEntry(v, k));
    return reverseMap;
  }
}

/// https://gist.github.com/venkatd/7125882a8e86d80000ea4c2da2c2a8ad
class Debouncer {
  final Duration delay;
  Timer _timer;

  Debouncer(this.delay);

  call(Function action) {
    _timer?.cancel();
    _timer = Timer(delay, action);
  }

  void dispose() {
    _timer = null;
  }
}