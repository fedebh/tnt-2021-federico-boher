class CurrentTime {
  static String getSeason(int month, int day) {
    if (month >= 1 && month < 3) return 'Winter';
    if (month > 3 && month < 6) return 'Spring';
    if (month > 6 && month < 9) return 'Summer';
    if (month > 9 && month < 12) return 'Fall';

    if (month == 3) return day < 22 ? 'Winter' : 'Spring';
    if (month == 6) return day < 22 ? 'Spring' : 'Summer';
    if (month == 9) return day < 22 ? 'Summer' : 'Fall';
    if (month == 12) return day < 22 ? 'Fall' : 'Winter';

    return '';
  }

  static String getNextSeason(int month, int day) {
    if (month >= 1 && month < 3) return 'Spring';
    if (month > 3 && month < 6) return 'Summer';
    if (month > 6 && month < 9) return 'Fall';
    if (month > 9 && month < 12) return 'Winter';

    if (month == 3) return day < 22 ? 'Spring' : 'Summer';
    if (month == 6) return day < 22 ? 'Summer' : 'Fall';
    if (month == 9) return day < 22 ? 'Fall' : 'Winter';
    if (month == 12) return day < 22 ? 'Winter' : 'Spring';

    return '';
  }

  static String getCurrentSeason() =>
      getSeason(DateTime.now().month, DateTime.now().day);

  static String getCurrentNextSeason() =>
      getNextSeason(DateTime.now().month, DateTime.now().day);

  static int getCurrentYear() => DateTime.now().year;

  static int getNextYear() => DateTime.now().year + 1;
}
