import 'package:actividad_05/bloc/favorites/favorites_bloc.dart';
import 'package:actividad_05/models/anime/anime.dart';
import 'package:actividad_05/models/comics/character.dart';
import 'package:actividad_05/models/comics/comic.dart';
import 'package:actividad_05/models/comics/creator.dart';
import 'package:actividad_05/models/comics/marvel_event.dart';
import 'package:actividad_05/models/comics/series.dart';
import 'package:actividad_05/modules/favorites/home/widgets/favorite_content.dart';
import 'package:actividad_05/routes/routes.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class FavoriteHomeScreen extends StatelessWidget {
  const FavoriteHomeScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<FavoritesBloc, FavoritesState>(
        builder: (context, state) {
      if (state is FavoritesEmptyState)
        return Scaffold(body: Center(child: Text(state.text)));

      if (state is FavoritesAllLoadedState) {
        return ListView(children: [
          FavoriteContent<Character>(
              label: 'Characters',
              detailsRoute: Routes.CHARACTER_DETAILS,
              list: state.map[Character]),
          FavoriteContent<Series>(
              label: 'Series',
              detailsRoute: Routes.SERIES_DETAILS,
              list: state.map[Series]),
          FavoriteContent<Comic>(
              label: 'Comics',
              detailsRoute: Routes.COMICS_DETAILS,
              list: state.map[Comic]),
          FavoriteContent<MarvelEvent>(
              label: 'Events',
              detailsRoute: Routes.EVENT_DETAILS,
              list: state.map[MarvelEvent]),
          FavoriteContent<Creator>(
              label: 'Creators',
              detailsRoute: Routes.CREATOR_DETAILS,
              list: state.map[Creator]),
          FavoriteContent<Anime>(
              label: 'Anime',
              detailsRoute: Routes.ANIME_DETAILS,
              list: state.map[Anime]),
        ]);
      }

      return SizedBox();
    });
  }
}
