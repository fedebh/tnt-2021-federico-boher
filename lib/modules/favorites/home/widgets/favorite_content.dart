import 'package:actividad_05/bloc/favorites/favorites_bloc.dart';
import 'package:actividad_05/models/anime/anime.dart';
import 'package:actividad_05/models/comics.dart';
import 'package:actividad_05/models/entity.dart';
import 'package:actividad_05/common/widgets/anime/anime_card_list\.dart';
import 'package:actividad_05/common/widgets/comics/thumb_hero_list.dart';
import 'package:actividad_05/routes/routes.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class FavoriteContent<T extends Entity> extends StatelessWidget {
  const FavoriteContent({Key key, this.detailsRoute, this.label, this.list})
      : super(key: key);

  final String detailsRoute;
  final String label;
  final List<T> list;

  onPressedIcon<T>(bloc, args) {
    final Map<dynamic, String> _routes = {
      Character: Routes.FAVORITE_CHARACTER_LIST,
      Series: Routes.FAVORITE_SERIES_LIST,
      Comic: Routes.FAVORITE_COMICS_LIST,
      MarvelEvent: Routes.FAVORITE_EVENT_LIST,
      Creator: Routes.FAVORITE_CREATOR_LIST,
      Anime: Routes.FAVORITE_ANIME_LIST
    };

    bloc..add(FavoritesWait());
    Get.toNamed(_routes[T], arguments: args)
        .then((value) => bloc..add(FavoritesLoadAll()));
  }

  onTapImage(bloc, index) {
    bloc..add(FavoritesWait());
    Get.toNamed(detailsRoute, arguments: index)
        .then((value) => bloc..add(FavoritesLoadAll()));
  }

  @override
  Widget build(BuildContext context) {
    FavoritesBloc _bloc = context.read<FavoritesBloc>();
    final Map<String, dynamic> _args = {
      'title': "Favorite $label",
      'detailsRoute': detailsRoute,
    };

    if (list.isEmpty) return SizedBox();

    if (T != Anime) {
      return ThumbHeroList(
          onPressedIcon: () => onPressedIcon<T>(_bloc, _args),
          onTapImage: (index) => onTapImage(_bloc, list[index]),
          label: label,
          list: list);
    } else {
      return AnimeCardList(
          onPressedIcon: () => onPressedIcon<T>(_bloc, _args),
          onTapImage: (index) => onTapImage(_bloc, list[index]),
          label: label,
          list: list as List<Anime>);
    }
  }
}
