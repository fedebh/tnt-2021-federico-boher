import 'package:actividad_05/bloc/favorites/favorites_bloc.dart';
import 'package:actividad_05/models/anime/anime.dart';
import 'package:actividad_05/common/widgets/anime/anime_card_grid.dart';
import 'package:actividad_05/repositories/favorites_repository.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';

class FavoriteAnimeListScreen extends StatelessWidget {
  const FavoriteAnimeListScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final dynamic _args = ModalRoute.of(context).settings.arguments as dynamic;
    final String _title = _args['title'];
    final String _detailsRoute = _args['detailsRoute'];

    _body(bloc, state) {
      if (state is FavoritesEmptyState) return Center(child: Text(state.text));

      if (state is FavoritesLoadedState<Anime>)
        return AnimeCardGrid(
            onTapImage: (index) {
              bloc..add(FavoritesWait());
              Get.toNamed(_detailsRoute, arguments: state.list[index])
                  .then((value) => bloc..add(FavoritesLoad<Anime>()));
            },
            list: state.list);

      return Scaffold(body: Center(child: Text('Error')));
    }

    return BlocProvider(
        create: (BuildContext context) =>
            FavoritesBloc(repository: context.read<FavoritesRepository>())
              ..add(FavoritesLoad<Anime>()),
        child: BlocBuilder<FavoritesBloc, FavoritesState>(
            builder: (context, state) {
          FavoritesBloc _bloc = context.read<FavoritesBloc>();
          if (state is FavoritesLoadingState)
            return Scaffold(body: Center(child: SizedBox()));

          return Scaffold(
              appBar: AppBar(
                iconTheme: IconThemeData(
                  color: Colors.black,
                ),
                backgroundColor: Colors.transparent,
                elevation: 0.0,
                centerTitle: true,
                title: Text(
                  _title ?? '',
                  style: TextStyle(color: Colors.black87),
                ),
              ),
              backgroundColor: Colors.white,
              body: _body(_bloc, state));
        }));
  }
}
