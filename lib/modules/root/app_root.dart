import 'package:actividad_05/bloc/favorites/favorites_bloc.dart';
import 'package:actividad_05/modules/anime/home/bloc/anime_home_bloc.dart';
import 'package:actividad_05/modules/favorites/home/screens/favorite_home_screen.dart';
import 'package:actividad_05/modules/search/home/bloc/search_bloc.dart';
import 'package:actividad_05/modules/search/home/screens/search_home_screen.dart';
import 'package:actividad_05/services/graphql/anime_gql_service/anime_gql_service.dart';
import 'package:actividad_05/services/graphql/anime_gql_service/queries/anime_gql_query.dart';
import 'package:actividad_05/utils/current_time.dart';
import 'package:actividad_05/utils/structs.dart';
import 'package:actividad_05/bloc/graphql/graphql_bloc.dart';
import 'package:actividad_05/modules/anime/home/screens/anime_home_screen.dart';
import 'package:actividad_05/modules/comics/home/bloc/comics_home_screen_bloc.dart';
import 'package:actividad_05/modules/comics/home/screens/comics_home_screen.dart';
import 'package:actividad_05/repositories/favorites_repository.dart';
import 'package:actividad_05/repositories/marvel_repository.dart';
import 'package:fancy_bottom_navigation/fancy_bottom_navigation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class AppRoot extends StatefulWidget {
  const AppRoot({Key key}) : super(key: key);

  @override
  _AppRootState createState() => _AppRootState();
}

class _AppRootState extends State<AppRoot> {
  int currentPage = 0;
  changePage(index) {
    setState(() {
      currentPage = index;
    });
  }

  /// Blocs que se instancian una sola vez
  initBlocProviders() {
    return MultiBlocProvider(providers: [
      BlocProvider(
          create: (BuildContext context) =>
              ComicsHomeScreenBloc(repository: context.read<MarvelRepository>())
                ..add(ComicsHomeScreenLoad())),
      BlocProvider(
          create: (BuildContext context) =>
              AnimeHomeBloc(service: context.read<AnimeGQLService>())
                ..add(GraphQLFetchData(ANIME_HOME_QUERY, variables: {
                  'season': CurrentTime.getCurrentSeason().toUpperCase(),
                  'seasonYear': CurrentTime.getCurrentYear(),
                  'nextSeason': CurrentTime.getCurrentNextSeason().toUpperCase()
                }))),
      BlocProvider(
          create: (BuildContext context) => SearchBloc(
              marvelRepository: context.read<MarvelRepository>(),
              animeService: context.read<AnimeGQLService>())..add(SearchInitialize())),
    ], child: pages[currentPage].item3);
  }

  /// **Tupla<Titulo, Tab, Screen>** \
  /// Blocs que se reinstancian cambiando tab
  final List<Tuple3> pages = [
    Tuple3('Comics', TabData(iconData: Icons.receipt, title: 'Comics'),
        ComicsHomeScreen()),
    Tuple3('Anime', TabData(iconData: Icons.tv_rounded, title: 'Anime'),
        AnimeHomeScreen()),
    Tuple3('Search', TabData(iconData: Icons.search, title: 'Search'),
        SearchHomeScreen()),
    Tuple3(
        'Favorites',
        TabData(iconData: Icons.favorite, title: 'Favorites'),
        BlocProvider(
            create: (BuildContext context) =>
                FavoritesBloc(repository: context.read<FavoritesRepository>())
                  ..add(FavoritesLoadAll()),
            child: FavoriteHomeScreen()))
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0.0,
        title: Text(
          pages[currentPage].item1,
          style: TextStyle(color: Colors.black87),
        ),
      ),
      body: initBlocProviders(),
      bottomNavigationBar: FancyBottomNavigation(
          tabs: pages.map<TabData>((page) => page.item2).toList(),
          onTabChangedListener: (index) => changePage(index)),
    );
  }
}
