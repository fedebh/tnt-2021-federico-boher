import 'package:actividad_05/app.dart';
import 'package:fancy_bottom_navigation/fancy_bottom_navigation.dart';
import 'package:flutter/material.dart';

class OfflineRoot extends StatelessWidget {
  const OfflineRoot({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0.0,
        title: Text(
          APP_TITLE,
          style: TextStyle(color: Colors.black87),
        ),
      ),
      body: Center(
          child: Text(
        'Internet is not available.\nPlease check your connection and restart the application.',
        textAlign: TextAlign.center,
      )),
      bottomNavigationBar:
          FancyBottomNavigation(onTabChangedListener: (index) => {}, tabs: [
        TabData(iconData: Icons.signal_wifi_connected_no_internet_4, title: 'Wifi is offline'),
        TabData(iconData: Icons.signal_cellular_connected_no_internet_4_bar, title: 'Mobile data is offline')
      ]),
    );
  }
}
