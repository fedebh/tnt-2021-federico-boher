import 'package:actividad_05/bloc/marvel_api/marvel_api_bloc.dart';
import 'package:actividad_05/models/comics.dart';
import 'package:actividad_05/models/entity.dart';

class LatestsListBloc extends MarvelAPIBloc {
  LatestsListBloc({repository}) : super(repository: repository);
  List<dynamic> list;

  @override
  Stream<MarvelAPIState> mapEventToState(MarvelAPIEvent event) async* {
    if (event is MarvelAPIGetInitializedData)
      yield* _mapInitialDataToStates(event);
    else if (event is MarvelAPIFetchNextPage<Comic>)
      yield* _mapFetchLatestsToStates<Comic>(event);
    else if (event is MarvelAPIFetchNextPage<MarvelEvent>)
      yield* _mapFetchLatestsToStates<MarvelEvent>(event);
    else if (event is MarvelAPIFetchNextPage<Creator>)
      yield* _mapFetchLatestsToStates<Creator>(event);
  }

  Stream<MarvelAPIState> _mapInitialDataToStates(
      MarvelAPIGetInitializedData event) async* {
    list = event.data;
    yield MarvelAPILoadSuccessState(event.data);
  }

  Stream<MarvelAPIState> _mapFetchLatestsToStates<T extends Entity>(
      MarvelAPIFetchNextPage<T> event) async* {
    if (isEndReached) return;

    yield MarvelAPILoadingState();
    final fetched = await repository.getAll<T>(queryParams: toNextPage());
    
    if (checkIfEndReached(fetched)) {
      yield MarvelAPIEndPageReachedState();
      return;
    }
    
    list.addAll(fetched.data.results);
    if(list.isEmpty)
      yield MarvelAPIEmptyState();
    else
      yield MarvelAPILoadSuccessState(list);
  }
}
