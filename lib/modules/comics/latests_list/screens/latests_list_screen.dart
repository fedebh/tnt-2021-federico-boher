import 'package:actividad_05/bloc/marvel_api/marvel_api_bloc.dart';
import 'package:actividad_05/models/entity.dart';
import 'package:actividad_05/modules/comics/latests_list/bloc/latests_list_bloc.dart';
import 'package:actividad_05/common/widgets/comics/thumb_hero_grid.dart';
import 'package:actividad_05/repositories/marvel_repository.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';

class LatestsListScreen<T extends Entity> extends StatelessWidget {
  const LatestsListScreen(this.title, {Key key, this.detailsRoute})
      : super(key: key);
  final String title;
  final String detailsRoute;

  @override
  Widget build(BuildContext context) {
    final List<T> _list = ModalRoute.of(context).settings.arguments as List<T>;
    dynamic _bloc;
    List<T> _data;
    bool _isLoading;

    return BlocProvider(
        create: (BuildContext context) =>
            LatestsListBloc(repository: context.read<MarvelRepository>())
              ..add(MarvelAPIGetInitializedData(_list)),
        child: BlocBuilder<LatestsListBloc, MarvelAPIState>(
            builder: (context, state) {
          _bloc = context.read<LatestsListBloc>();
          _isLoading = state is MarvelAPILoadingState;

          if (state is MarvelAPILoadSuccessState) _data = state.data;

          return Stack(children: [
            Scaffold(
                appBar: AppBar(
                  iconTheme: IconThemeData(
                    color: Colors.black,
                  ),
                  backgroundColor: Colors.transparent,
                  elevation: 0.0,
                  centerTitle: true,
                  title: Text(
                    title ?? '',
                    style: TextStyle(color: Colors.black87),
                  ),
                ),
                backgroundColor: Colors.white,
                body: ThumbHeroGrid(
                    onEndReached: (endPosition) =>
                        _bloc..add(MarvelAPIFetchNextPage<T>()),
                    onTapImage: (index) =>
                        Get.toNamed(detailsRoute, arguments: _data[index]),
                    list: _data)),
            _isLoading
                ? Center(
                    child: Container(
                        child: CircularProgressIndicator(),
                        height: 60,
                        width: 60,
                        padding: const EdgeInsets.all(10),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(100),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.black54,
                              blurRadius: 6,
                              offset: Offset(0, 4),
                            ),
                          ],
                        )))
                : SizedBox()
          ]);
        }));
  }
}
