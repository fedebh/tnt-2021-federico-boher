import 'package:actividad_05/bloc/marvel_api/marvel_api_bloc.dart';
import 'package:actividad_05/common/templates/details/screens/detail_screen_template.dart';
import 'package:actividad_05/utils/structs.dart';
import 'package:actividad_05/common/widgets/loading_related_content.dart';
import 'package:actividad_05/models/comics/series.dart';
import 'package:actividad_05/common/widgets/comics/marvel_attribution.dart';
import 'package:actividad_05/modules/comics/details/bloc/detail_screen_bloc.dart';
import 'package:actividad_05/modules/comics/details/widgets/comics_bottom_modal_sheet.dart';
import 'package:actividad_05/modules/comics/details/widgets/detail_hero_with_back_btn.dart';
import 'package:actividad_05/modules/comics/details/widgets/related_content_list.dart';
import 'package:actividad_05/routes/routes.dart';
import 'package:actividad_05/repositories/marvel_repository.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class SeriesDetailScreen extends StatelessWidget {
  const SeriesDetailScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final Series series = ModalRoute.of(context).settings.arguments as Series;
    final _seriesTitle = series.title;
    return DetailScreenTemplate<Series>(
        entity: series,
        header: DetailHeroWithBackBtn(thumbnail: series.thumbnail),
        title: _seriesTitle,
        description: series.description,
        share: Pair(comicsBottomModalSheet, series.urls),
        relatedContent: BlocProvider<DetailScreenBloc>(
          create: (BuildContext context) =>
              DetailScreenBloc(repository: context.read<MarvelRepository>())
              ..add(DetailScreenLoad<Series>(series.id)),
          child: BlocBuilder<DetailScreenBloc, MarvelAPIState>(
              builder: (context, state) {
            if (state is MarvelAPILoadingState) return LoadingRelatedContent();

            if (state is DetailScreenSeriesLoadedState) {
              return Column(children: [
                RelatedContentList(
                    label: 'Characters',
                    title: '$_seriesTitle characters',
                    iconRoute: Routes.SERIES_RELATED_CHARACTERS,
                    thumbnailRoute: Routes.CHARACTER_DETAILS,
                    id: series.id,
                    content: state.characters),
                RelatedContentList(
                    label: 'Comics',
                    title: '$_seriesTitle comics',
                    iconRoute: Routes.SERIES_RELATED_COMICS,
                    thumbnailRoute: Routes.COMICS_DETAILS,
                    id: series.id,
                    content: state.comics),
                RelatedContentList(
                    label: 'Events',
                    title: '$_seriesTitle events',
                    iconRoute: Routes.SERIES_RELATED_EVENTS,
                    thumbnailRoute: Routes.EVENT_DETAILS,
                    id: series.id,
                    content: state.events),
                RelatedContentList(
                    label: 'Creators',
                    title: '$_seriesTitle creators',
                    iconRoute: Routes.SERIES_RELATED_CREATORS,
                    thumbnailRoute: Routes.CREATOR_DETAILS,
                    id: series.id,
                    content: state.creators),
              ]);
            }

            return SizedBox();
          }),
        ),
        footer: MarvelAttribution());
  }
}
