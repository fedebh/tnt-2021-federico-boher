import 'package:actividad_05/bloc/marvel_api/marvel_api_bloc.dart';
import 'package:actividad_05/common/templates/details/screens/detail_screen_template.dart';
import 'package:actividad_05/utils/structs.dart';
import 'package:actividad_05/modules/comics/details/widgets/comics_bottom_modal_sheet.dart';
import 'package:actividad_05/modules/comics/details/widgets/detail_hero_with_back_btn.dart';
import 'package:actividad_05/modules/comics/details/bloc/detail_screen_bloc.dart';
import 'package:actividad_05/models/comics/creator.dart';
import 'package:actividad_05/common/widgets/comics/marvel_attribution.dart';
import 'package:actividad_05/common/widgets/loading_related_content.dart';
import 'package:actividad_05/modules/comics/details/widgets/related_content_list.dart';
import 'package:actividad_05/routes/routes.dart';
import 'package:actividad_05/repositories/marvel_repository.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class CreatorDetailScreen extends StatelessWidget {
  const CreatorDetailScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final Creator creator =
        ModalRoute.of(context).settings.arguments as Creator;
    final _creatorFullName = creator.fullName;

    return DetailScreenTemplate<Creator>(
      entity: creator,
      header: DetailHeroWithBackBtn(thumbnail: creator.thumbnail),
      title: _creatorFullName,
      description: '',
      share: Pair(comicsBottomModalSheet, creator.urls),
      relatedContent: BlocProvider<DetailScreenBloc>(
        create: (BuildContext context) =>
            DetailScreenBloc(repository: context.read<MarvelRepository>())
            ..add(DetailScreenLoad<Creator>(creator.id)),
        child: BlocBuilder<DetailScreenBloc, MarvelAPIState>(
            builder: (context, state) {
          if (state is MarvelAPILoadingState) return LoadingRelatedContent();

          if (state is DetailScreenCreatorLoadedState) {
            return Column(children: [
              RelatedContentList(
                  label: 'Series',
                  title: '$_creatorFullName series',
                  iconRoute: Routes.CREATOR_RELATED_SERIES,
                  thumbnailRoute: Routes.SERIES_DETAILS,
                  id: creator.id,
                  content: state.series),
              RelatedContentList(
                  label: 'Comics',
                  title: '$_creatorFullName comics',
                  iconRoute: Routes.CREATOR_RELATED_COMICS,
                  thumbnailRoute: Routes.COMICS_DETAILS,
                  id: creator.id,
                  content: state.comics),
              RelatedContentList(
                  label: 'Events',
                  title: '$_creatorFullName events',
                  iconRoute: Routes.CREATOR_RELATED_EVENTS,
                  thumbnailRoute: Routes.EVENT_DETAILS,
                  id: creator.id,
                  content: state.events),
            ]);
          }

          return SizedBox();
        }),
      ),
      footer: MarvelAttribution(),
    );
  }
}
