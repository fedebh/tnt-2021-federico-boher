import 'package:actividad_05/bloc/marvel_api/marvel_api_bloc.dart';
import 'package:actividad_05/common/templates/details/screens/detail_screen_template.dart';
import 'package:actividad_05/utils/structs.dart';
import 'package:actividad_05/common/widgets/loading_related_content.dart';
import 'package:actividad_05/models/comics/marvel_event.dart';
import 'package:actividad_05/common/widgets/comics/marvel_attribution.dart';
import 'package:actividad_05/modules/comics/details/bloc/detail_screen_bloc.dart';
import 'package:actividad_05/modules/comics/details/widgets/comics_bottom_modal_sheet.dart';
import 'package:actividad_05/modules/comics/details/widgets/detail_hero_with_back_btn.dart';
import 'package:actividad_05/modules/comics/details/widgets/related_content_list.dart';
import 'package:actividad_05/routes/routes.dart';
import 'package:actividad_05/repositories/marvel_repository.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class EventDetailScreen extends StatelessWidget {
  const EventDetailScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final MarvelEvent event =
        ModalRoute.of(context).settings.arguments as MarvelEvent;
    final _eventTitle = event.title;

    return DetailScreenTemplate<MarvelEvent>(
        entity: event,
        header: DetailHeroWithBackBtn(thumbnail: event.thumbnail),
        title: _eventTitle,
        description: event.description,
        share: Pair(comicsBottomModalSheet, event.urls),
        relatedContent: BlocProvider<DetailScreenBloc>(
          create: (BuildContext context) =>
              DetailScreenBloc(repository: context.read<MarvelRepository>())
              ..add(DetailScreenLoad<MarvelEvent>(event.id)),
          child: BlocBuilder<DetailScreenBloc, MarvelAPIState>(
              builder: (context, state) {
            if (state is MarvelAPILoadingState) return LoadingRelatedContent();

            if (state is DetailScreenEventLoadedState) {
              return Column(children: [
                RelatedContentList(
                    label: 'Characters',
                    title: '$_eventTitle characters',
                    iconRoute: Routes.EVENT_RELATED_CHARACTERS,
                    thumbnailRoute: Routes.CHARACTER_DETAILS,
                    id: event.id,
                    content: state.characters),
                RelatedContentList(
                    label: 'Series',
                    title: '$_eventTitle series',
                    iconRoute: Routes.EVENT_RELATED_SERIES,
                    thumbnailRoute: Routes.SERIES_DETAILS,
                    id: event.id,
                    content: state.series),
                RelatedContentList(
                    label: 'Comics',
                    title: '$_eventTitle comics',
                    iconRoute: Routes.EVENT_RELATED_COMICS,
                    thumbnailRoute: Routes.COMICS_DETAILS,
                    id: event.id,
                    content: state.comics),
                RelatedContentList(
                    label: 'Creators',
                    title: '$_eventTitle creators',
                    iconRoute: Routes.EVENT_RELATED_CREATORS,
                    thumbnailRoute: Routes.CREATOR_DETAILS,
                    id: event.id,
                    content: state.creators),
              ]);
            }

            return SizedBox();
          }),
        ),
        footer: MarvelAttribution());
  }
}
