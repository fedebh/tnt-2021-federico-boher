import 'package:actividad_05/bloc/marvel_api/marvel_api_bloc.dart';
import 'package:actividad_05/utils/structs.dart';
import 'package:actividad_05/modules/comics/details/widgets/comics_bottom_modal_sheet.dart';
import 'package:actividad_05/modules/comics/details/widgets/detail_hero_with_back_btn.dart';
import 'package:actividad_05/modules/comics/details/bloc/detail_screen_bloc.dart';
import 'package:actividad_05/common/templates/details/screens/detail_screen_template.dart';
import 'package:actividad_05/models/comics/character.dart';
import 'package:actividad_05/common/widgets/comics/marvel_attribution.dart';
import 'package:actividad_05/common/widgets/loading_related_content.dart';
import 'package:actividad_05/modules/comics/details/widgets/related_content_list.dart';
import 'package:actividad_05/routes/routes.dart';
import 'package:actividad_05/repositories/marvel_repository.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class CharacterDetailScreen extends StatelessWidget {
  const CharacterDetailScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final Character character =
        ModalRoute.of(context).settings.arguments as Character;

    final String _characterName = character.name;

    return DetailScreenTemplate<Character>(
      entity: character,
      header: DetailHeroWithBackBtn(thumbnail: character.thumbnail),
      title: _characterName,
      description: character.description,
      share: Pair(comicsBottomModalSheet, character.urls),
      relatedContent: BlocProvider<DetailScreenBloc>(
        create: (BuildContext context) =>
            DetailScreenBloc(repository: context.read<MarvelRepository>())
              ..add(DetailScreenLoad<Character>(character.id)),
        child: BlocBuilder<DetailScreenBloc, MarvelAPIState>(
            builder: (context, state) {
          
          if (state is MarvelAPILoadingState) return LoadingRelatedContent();

          if (state is DetailScreenCharacterLoadedState) {
            return Column(children: [
              RelatedContentList(
                  label: 'Comics',
                  title: '$_characterName comics',
                  iconRoute: Routes.CHARACTER_RELATED_COMICS,
                  thumbnailRoute: Routes.COMICS_DETAILS,
                  id: character.id,
                  content: state.comics),
              RelatedContentList(
                  label: 'Series',
                  title: '$_characterName series',
                  iconRoute: Routes.CHARACTER_RELATED_SERIES,
                  thumbnailRoute: Routes.SERIES_DETAILS,
                  id: character.id,
                  content: state.series),
              RelatedContentList(
                  label: 'Events',
                  title: '$_characterName events',
                  iconRoute: Routes.CHARACTER_RELATED_EVENTS,
                  thumbnailRoute: Routes.EVENT_DETAILS,
                  id: character.id,
                  content: state.events),
            ]);
          }

          return SizedBox();
        }),
      ),
      footer: MarvelAttribution(),
    );
  }
}
