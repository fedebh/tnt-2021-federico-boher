import 'package:actividad_05/bloc/marvel_api/marvel_api_bloc.dart';
import 'package:actividad_05/common/templates/details/screens/detail_screen_template.dart';
import 'package:actividad_05/utils/structs.dart';
import 'package:actividad_05/modules/comics/details/widgets/comics_bottom_modal_sheet.dart';
import 'package:actividad_05/modules/comics/details/widgets/detail_hero_with_back_btn.dart';
import 'package:actividad_05/modules/comics/details/bloc/detail_screen_bloc.dart';
import 'package:actividad_05/models/comics/comic.dart';
import 'package:actividad_05/common/widgets/comics/marvel_attribution.dart';
import 'package:actividad_05/common/widgets/loading_related_content.dart';
import 'package:actividad_05/modules/comics/details/widgets/related_content_list.dart';
import 'package:actividad_05/routes/routes.dart';
import 'package:actividad_05/repositories/marvel_repository.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ComicsDetailScreen extends StatelessWidget {
  const ComicsDetailScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final Comic comic = ModalRoute.of(context).settings.arguments as Comic;
    final _comicTitle = comic.title;

    return DetailScreenTemplate<Comic>(
      entity: comic,
      header: DetailHeroWithBackBtn(thumbnail: comic.thumbnail),
      title: _comicTitle,
      description: comic.description,
      share: Pair(comicsBottomModalSheet, comic.urls),
      relatedContent: BlocProvider<DetailScreenBloc>(
        create: (BuildContext context) =>
            DetailScreenBloc(repository: context.read<MarvelRepository>())
              ..add(DetailScreenLoad<Comic>(comic.id)),
        child: BlocBuilder<DetailScreenBloc, MarvelAPIState>(
            builder: (context, state) {
          if (state is MarvelAPILoadingState) return LoadingRelatedContent();

          if (state is DetailScreenComicsLoadedState) {
            return Column(children: [
              RelatedContentList(
                  label: 'Characters',
                  title: '$_comicTitle comics',
                  iconRoute: Routes.COMIC_RELATED_CHARACTERS,
                  thumbnailRoute: Routes.CHARACTER_DETAILS,
                  id: comic.id,
                  content: state.characters),
              RelatedContentList(
                  label: 'Events',
                  title: '$_comicTitle events',
                  iconRoute: Routes.COMIC_RELATED_EVENTS,
                  thumbnailRoute: Routes.EVENT_DETAILS,
                  id: comic.id,
                  content: state.events),
              RelatedContentList(
                  label: 'Creators',
                  title: '$_comicTitle events',
                  iconRoute: Routes.COMIC_RELATED_CREATORS,
                  thumbnailRoute: Routes.CREATOR_DETAILS,
                  id: comic.id,
                  content: state.creators),
            ]);
          }

          return SizedBox();
        }),
      ),
      footer: MarvelAttribution(),
    );
  }
}
