part of 'detail_screen_bloc.dart';

class DetailScreenLoad<T extends Entity> extends MarvelAPIEvent {
  const DetailScreenLoad(this.id);
  final id;
}