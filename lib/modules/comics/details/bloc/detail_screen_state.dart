part of 'detail_screen_bloc.dart';

class DetailScreenEventLoadedState extends MarvelAPIState {
  const DetailScreenEventLoadedState({this.characters, this.series, this.comics, this.creators});

  final MarvelResponse<Character> characters;
  final MarvelResponse<Series> series;
  final MarvelResponse<Comic> comics;
  final MarvelResponse<Creator> creators;
}

class DetailScreenCharacterLoadedState extends MarvelAPIState {
  const DetailScreenCharacterLoadedState({this.comics, this.series, this.events});

  final MarvelResponse<Comic> comics;
  final MarvelResponse<Series> series;
  final MarvelResponse<MarvelEvent> events;

  @override
  List<Object> get props => [comics, series, events];
}

class DetailScreenCreatorLoadedState extends MarvelAPIState {
  const DetailScreenCreatorLoadedState({this.comics, this.series, this.events});
  
  final MarvelResponse<Comic> comics;
  final MarvelResponse<Series> series;
  final MarvelResponse<MarvelEvent> events;
}

class DetailScreenSeriesLoadedState extends MarvelAPIState {
  const DetailScreenSeriesLoadedState({this.characters, this.comics, this.events,  this.creators});
  
  final MarvelResponse<Character> characters;
  final MarvelResponse<Comic> comics;
  final MarvelResponse<MarvelEvent> events;
  final MarvelResponse<Creator> creators;
}

class DetailScreenComicsLoadedState extends MarvelAPIState {
  const DetailScreenComicsLoadedState({this.characters, this.events,  this.creators});
  
  final MarvelResponse<Character> characters;
  final MarvelResponse<MarvelEvent> events;
  final MarvelResponse<Creator> creators;
}