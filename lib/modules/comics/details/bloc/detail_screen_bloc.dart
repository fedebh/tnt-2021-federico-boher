import 'package:actividad_05/bloc/marvel_api/marvel_api_bloc.dart';
import 'package:actividad_05/models/models.dart';
part 'detail_screen_state.dart';
part 'detail_screen_event.dart';

class DetailScreenBloc extends MarvelAPIBloc {
  DetailScreenBloc({repository}) : super(repository: repository);

  @override
  Stream<MarvelAPIState> mapEventToState(MarvelAPIEvent event) async* {
    if (event is DetailScreenLoad<Character>)
      yield* _mapLoadCharacterToStates(event);
    else if (event is DetailScreenLoad<Series>)
      yield* _mapLoadSeriesToState(event);
    else if (event is DetailScreenLoad<Comic>)
      yield* _mapLoadComicToState(event);
    else if (event is DetailScreenLoad<Creator>)
      yield* _mapLoadCreatorToState(event);
    else if (event is DetailScreenLoad<MarvelEvent>)
      yield* _mapLoadMarvelEventToStates(event);
  }

  Stream<MarvelAPIState> _mapLoadCharacterToStates(
      DetailScreenLoad<Character> event) async* {
    comics = await repository.getRelated<Character, Comic>(event.id);
    series = await repository.getRelated<Character, Series>(event.id);
    events = await repository.getRelated<Character, MarvelEvent>(event.id);
    
    if (MarvelAPIBloc.areEmptyResponses([comics, series, events]))
      yield MarvelAPIEmptyState();
    else
      yield DetailScreenCharacterLoadedState(
          comics: comics, series: series, events: events);
  }

  Stream<MarvelAPIState> _mapLoadSeriesToState(
      DetailScreenLoad<Series> event) async* {
    characters = await repository.getRelated<Series, Character>(event.id);
    comics = await repository.getRelated<Series, Comic>(event.id);
    events = await repository.getRelated<Series, MarvelEvent>(event.id);
    creators = await repository.getRelated<Series, Creator>(event.id);

    if (MarvelAPIBloc.areEmptyResponses([characters, comics, events, creators]))
      yield MarvelAPIEmptyState();
    else
      yield DetailScreenSeriesLoadedState(
          characters: characters,
          comics: comics,
          events: events,
          creators: creators);
  }

  Stream<MarvelAPIState> _mapLoadComicToState(
      DetailScreenLoad<Comic> event) async* {
    characters = await repository.getRelated<Series, Character>(event.id);
    events = await repository.getRelated<Series, MarvelEvent>(event.id);
    creators = await repository.getRelated<Series, Creator>(event.id);
    
    if (MarvelAPIBloc.areEmptyResponses([characters, events, creators]))
      yield MarvelAPIEmptyState();
    else
      yield DetailScreenSeriesLoadedState(
          characters: characters, events: events, creators: creators);
  }

  Stream<MarvelAPIState> _mapLoadCreatorToState(
      DetailScreenLoad<Creator> event) async* {
    comics = await repository.getRelated<Creator, Comic>(event.id);
    series = await repository.getRelated<Creator, Series>(event.id);
    events = await repository.getRelated<Creator, MarvelEvent>(event.id);
    
    if (MarvelAPIBloc.areEmptyResponses([comics, series, events]))
      yield MarvelAPIEmptyState();
    else
      yield DetailScreenCreatorLoadedState(
          comics: comics, series: series, events: events);
  }

  Stream<MarvelAPIState> _mapLoadMarvelEventToStates(
      DetailScreenLoad<MarvelEvent> event) async* {
    characters = await repository.getRelated<MarvelEvent, Character>(event.id);
    series = await repository.getRelated<MarvelEvent, Series>(event.id);
    comics = await repository.getRelated<MarvelEvent, Comic>(event.id);
    creators = await repository.getRelated<MarvelEvent, Creator>(event.id);

    if (MarvelAPIBloc.areEmptyResponses([characters, series, comics, creators]))
      yield MarvelAPIEmptyState();
    else
      yield DetailScreenEventLoadedState(
          characters: characters,
          series: series,
          comics: comics,
          creators: creators);
  }
}
