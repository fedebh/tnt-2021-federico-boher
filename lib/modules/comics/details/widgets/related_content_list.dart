import 'package:actividad_05/common/widgets/comics/thumb_hero_list\.dart';
import 'package:actividad_05/models/comics/marvel_response.dart';
import 'package:actividad_05/utils/structs.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class RelatedContentList extends StatelessWidget {
  const RelatedContentList(
      {Key key,
      this.title,
      this.id,
      this.content,
      this.label,
      this.thumbnailRoute,
      this.iconRoute})
      : super(key: key);

  final MarvelResponse<dynamic> content;
  final int id;
  final String label;
  final String iconRoute;
  final String thumbnailRoute;
  final String title;

  @override
  Widget build(BuildContext context) {
    if (content.data.results.length > 0)
      return ThumbHeroList(
          label: label,
          onPressedIcon: () => Get.toNamed(iconRoute, arguments: {
                'title': title,
                'detailsRoute': thumbnailRoute,
                'data': Pair(id, content.data.results)
              }),
          onTapImage: (index) => Get.toNamed(thumbnailRoute,
              arguments: content.data.results[index]),
          list: content.data.results);
    else
      return SizedBox();
  }
}
