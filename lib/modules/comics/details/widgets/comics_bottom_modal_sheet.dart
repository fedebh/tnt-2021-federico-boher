import 'package:actividad_05/models/comics/marvel_url.dart';
import 'package:flutter/material.dart';
import 'package:share_plus/share_plus.dart';

comicsBottomModalSheet(context, links) {
  if (links == null) {
    return SnackBar(content: Text("No links to share"));
  }

  listTile(context, title, link) {
    return ListTile(
      leading: Icon(Icons.link),
      title: Text(title),
      onTap: () {
        Share.share(link);
        Navigator.pop(context);
      },
    );
  }

  getType(MarvelUrlType url) {
    String result = urlTypeNames.reverse[url] ?? 'Link';
    return result;
  }

  return showModalBottomSheet(
      context: context,
      builder: (context) {
        return Wrap(
            children: links
                .map<ListTile>(
                    (item) => listTile(context, getType(item.type), item.url))
                .toList());
      });
}
