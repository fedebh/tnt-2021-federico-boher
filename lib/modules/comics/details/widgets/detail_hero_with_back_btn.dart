import 'package:actividad_05/models/comics/thumbnail.dart';
import 'package:flutter/material.dart';

class DetailHeroWithBackBtn extends StatelessWidget {
  const DetailHeroWithBackBtn({
    Key key,
    @required this.thumbnail,
  }) : super(key: key);

  final Thumbnail thumbnail;

  @override
  Widget build(BuildContext context) {
    return Container(
        width: double.infinity,
        height:
            thumbnailSizeDimensions[ThumbnailSize.PORTRAIT_INCREDIBLE].height,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
              bottomLeft: Radius.elliptical(60, 40),
              bottomRight: Radius.elliptical(60, 40)),
          boxShadow: [
            BoxShadow(
              color: Colors.black87,
              blurRadius: 10,
              offset: Offset(0, 4),
            ),
          ],
          image: DecorationImage(
            fit: BoxFit.cover,
            image: NetworkImage(
                this.thumbnail.getSizedThumb(ThumbnailSize.LANDSCAPE_XLARGE)),
          ),
        ),
        child: Row(children: [
          Expanded(
            child: Align(
              alignment: FractionalOffset.topLeft,
              child: IconButton(
                  onPressed: () => Navigator.pop(context),
                  iconSize: 30.0,
                  icon: Icon(
                    Icons.arrow_back,
                    color: Colors.white,
                  )),
            ),
          ),
        ]));
  }
}
