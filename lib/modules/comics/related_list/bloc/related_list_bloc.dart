import 'package:actividad_05/bloc/marvel_api/marvel_api_bloc.dart';
import 'package:actividad_05/models/comics.dart';
import 'package:actividad_05/models/entity.dart';

class RelatedListBloc<T extends Entity> extends MarvelAPIBloc {
  RelatedListBloc({repository}) : super(repository: repository);

  List<dynamic> list;
  int id;

  @override
  Stream<MarvelAPIState> mapEventToState(MarvelAPIEvent event) async* {
    if (event is MarvelAPIGetInitializedData)
      yield* mapInitialDataToStates(event);
    else
      yield* onNextFetchPageEvent(event);
  }

  Stream<MarvelAPIState> mapInitialDataToStates(
      MarvelAPIGetInitializedData event) async* {
    id = event.data.item1;
    list = event.data.item2;
    yield MarvelAPILoadSuccessState(list);
  }

  Stream<MarvelAPIState> onNextFetchPageEvent(MarvelAPIEvent event) async* {
    switch (T) {
      case Character:
        yield* onCharacterEvent(event);
        break;
      case Series:
        yield* onSeriesEvent(event);
        break;
      case Comic:
        yield* onComicsEvent(event);
        break;
      case MarvelEvent:
        yield* onMarvelEventEvent(event);
        break;
      case Creator:
        yield* onCreatorEvent(event);
        break;
    }
  }

  Stream<MarvelAPIState>
      _mapFetchRelatedToStates<TEntity extends Entity, TRelated extends Entity>(
          MarvelAPIFetchNextPage<TRelated> event) async* {
    if (isEndReached) return;

    yield MarvelAPILoadingState();
    final fetched = await repository.getRelated<TEntity, TRelated>(id,
        queryParams: toNextPage());

    if (checkIfEndReached(fetched)) {
      yield MarvelAPIEndPageReachedState();
      return;
    }

    list.addAll(fetched.data.results);
    if (list.isEmpty)
      yield MarvelAPIEmptyState();
    else
      yield MarvelAPILoadSuccessState(list);
  }

  Stream<MarvelAPIState> onCharacterEvent(MarvelAPIEvent event) async* {
    if (event is MarvelAPIFetchNextPage<Series>)
      yield* _mapFetchRelatedToStates<Character, Series>(event);
    else if (event is MarvelAPIFetchNextPage<Comic>)
      yield* _mapFetchRelatedToStates<Character, Comic>(event);
    else if (event is MarvelAPIFetchNextPage<MarvelEvent>)
      yield* _mapFetchRelatedToStates<Character, MarvelEvent>(event);
  }

  Stream<MarvelAPIState> onSeriesEvent(MarvelAPIEvent event) async* {
    if (event is MarvelAPIFetchNextPage<Character>)
      yield* _mapFetchRelatedToStates<Series, Character>(event);
    else if (event is MarvelAPIFetchNextPage<Comic>)
      yield* _mapFetchRelatedToStates<Series, Comic>(event);
    else if (event is MarvelAPIFetchNextPage<MarvelEvent>)
      yield* _mapFetchRelatedToStates<Series, MarvelEvent>(event);
    else if (event is MarvelAPIFetchNextPage<Creator>)
      yield* _mapFetchRelatedToStates<Series, Creator>(event);
  }

  Stream<MarvelAPIState> onComicsEvent(MarvelAPIEvent event) async* {
    if (event is MarvelAPIFetchNextPage<Character>)
      yield* _mapFetchRelatedToStates<Comic, Character>(event);
    else if (event is MarvelAPIFetchNextPage<MarvelEvent>)
      yield* _mapFetchRelatedToStates<Comic, MarvelEvent>(event);
    else if (event is MarvelAPIFetchNextPage<Creator>)
      yield* _mapFetchRelatedToStates<Comic, Creator>(event);
  }

  Stream<MarvelAPIState> onMarvelEventEvent(MarvelAPIEvent event) async* {
    if (event is MarvelAPIFetchNextPage<Character>)
      yield* _mapFetchRelatedToStates<MarvelEvent, Character>(event);
    else if (event is MarvelAPIFetchNextPage<Series>)
      yield* _mapFetchRelatedToStates<MarvelEvent, Series>(event);
    else if (event is MarvelAPIFetchNextPage<Comic>)
      yield* _mapFetchRelatedToStates<MarvelEvent, Comic>(event);
    else if (event is MarvelAPIFetchNextPage<Creator>)
      yield* _mapFetchRelatedToStates<MarvelEvent, Creator>(event);
  }

  Stream<MarvelAPIState> onCreatorEvent(MarvelAPIEvent event) async* {
    if (event is MarvelAPIFetchNextPage<Series>)
      yield* _mapFetchRelatedToStates<Creator, Series>(event);
    else if (event is MarvelAPIFetchNextPage<Comic>)
      yield* _mapFetchRelatedToStates<Creator, Comic>(event);
    else if (event is MarvelAPIFetchNextPage<MarvelEvent>)
      yield* _mapFetchRelatedToStates<Creator, MarvelEvent>(event);
  }
}
