import 'package:actividad_05/bloc/marvel_api/marvel_api_bloc.dart';
import 'package:actividad_05/models/entity.dart';
import 'package:actividad_05/common/widgets/comics/thumb_hero_grid.dart';
import 'package:actividad_05/modules/comics/related_list/bloc/related_list_bloc.dart';
import 'package:actividad_05/repositories/marvel_repository.dart';
import 'package:actividad_05/utils/structs.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';

class RelatedListScreen<TEntity extends Entity, TRelated extends Entity>
    extends StatelessWidget {
  const RelatedListScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final dynamic _args = ModalRoute.of(context).settings.arguments as dynamic;
    String _title = _args['title'];
    Pair<dynamic, List<dynamic>> _pair = _args['data'];
    String _detailsRoute = _args['detailsRoute'];
    dynamic _bloc;
    bool _isLoading;
    List<TRelated> _data;

    return BlocProvider(
        create: (BuildContext context) => RelatedListBloc<TEntity>(
            repository: context.read<MarvelRepository>())
          ..add(MarvelAPIGetInitializedData(_pair)),
        child: BlocBuilder<RelatedListBloc<TEntity>, MarvelAPIState>(
            builder: (context, state) {
          _bloc = context.read<RelatedListBloc<TEntity>>();
          _isLoading = state is MarvelAPILoadingState;

          if (state is MarvelAPILoadSuccessState) _data = state.data;

          return Stack(children: [
            Scaffold(
                appBar: AppBar(
                  iconTheme: IconThemeData(
                    color: Colors.black,
                  ),
                  backgroundColor: Colors.transparent,
                  elevation: 0.0,
                  centerTitle: true,
                  title: Text(
                    _title ?? '',
                    style: TextStyle(color: Colors.black87),
                  ),
                ),
                backgroundColor: Colors.white,
                body: ThumbHeroGrid(
                    onEndReached: (endPosition) =>
                        _bloc..add(MarvelAPIFetchNextPage<TRelated>()),
                    onTapImage: (index) =>
                        Get.toNamed(_detailsRoute, arguments: _data[index]),
                    list: _data)),
            _isLoading
                ? Center(
                    child: Container(
                        child: CircularProgressIndicator(),
                        height: 60,
                        width: 60,
                        padding: const EdgeInsets.all(10),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(100),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.black54,
                              blurRadius: 6,
                              offset: Offset(0, 4),
                            ),
                          ],
                        )))
                : SizedBox()
          ]);
        }));
  }
}
