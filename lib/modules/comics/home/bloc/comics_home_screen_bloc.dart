import 'package:actividad_05/bloc/marvel_api/marvel_api_bloc.dart';
import 'package:actividad_05/models/comics.dart';
part 'comics_home_screen_state.dart';
part 'comics_home_screen_event.dart';

class ComicsHomeScreenBloc extends MarvelAPIBloc {
  ComicsHomeScreenBloc({repository}) : super(repository: repository);

  @override
  Stream<MarvelAPIState> mapEventToState(MarvelAPIEvent event) async* {
    if (event is ComicsHomeScreenLoad)
      yield* _mapLoadAllToStates(event);
  }

  Stream<MarvelAPIState> _mapLoadAllToStates(
      ComicsHomeScreenLoad event) async* {
    
    bool connection = await canConnect();
    if(!connection)
      yield MarvelAPILoadFailState();

    characters = await repository.getAll<Character>();
    series = await repository.getAll<Series>();
    comics = await repository.getAll<Comic>();
    creators = await repository.getAll<Creator>();
    events = await repository.getAll<MarvelEvent>();

    if (MarvelAPIBloc.areEmptyResponses([characters, series, comics, creators, events]))
      yield MarvelAPIEmptyState();
    else
      yield ComicsHomeScreenLoadedState(
          characters: characters,
          series: series,
          comics: comics,
          creators: creators,
          events: events);
  }
}