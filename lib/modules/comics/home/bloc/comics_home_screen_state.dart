part of 'comics_home_screen_bloc.dart';

class ComicsHomeScreenLoadedState extends MarvelAPIState {
  ComicsHomeScreenLoadedState({this.characters, this.series, this.comics, this.events, this.creators});

  final MarvelResponse<Character> characters;
  final MarvelResponse<Series> series;
  final MarvelResponse<Comic> comics;
  final MarvelResponse<MarvelEvent> events;
  final MarvelResponse<Creator> creators;

  @override
  List<Object> get props => [characters, series, comics, events, creators];
}
