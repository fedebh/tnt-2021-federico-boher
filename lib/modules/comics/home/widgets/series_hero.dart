import 'package:actividad_05/models/comics/series.dart';
import 'package:actividad_05/models/comics/thumbnail.dart';
import 'package:flutter/material.dart';

class SeriesHero extends StatelessWidget {
  const SeriesHero({Key key, this.series, this.onTap}) : super(key: key);

  final Series series;
  final Function onTap;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: () => onTap(),
        child: Container(
            width: thumbnailSizeDimensions[ThumbnailSize.LANDSCAPE_SMALL].width,
            height:
                thumbnailSizeDimensions[ThumbnailSize.LANDSCAPE_SMALL].height,
            margin: EdgeInsets.all(10),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(10),
              boxShadow: [
                BoxShadow(
                  color: Colors.red.shade800,
                  blurRadius: 8,
                  offset: Offset(0, 2),
                ),
              ],
            ),
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                gradient: LinearGradient(
                  begin: Alignment.bottomCenter,
                  end: Alignment.topCenter,
                  colors: [
                    Colors.red.shade400,
                    Colors.red.shade800,
                  ],
                ),
              ),
              child: Center(
                child: Container(
                  padding: EdgeInsets.all(5),
                  child: Text(series.title,
                      textAlign: TextAlign.center,
                      style: new TextStyle(
                        fontSize: 10,
                        fontWeight: FontWeight.bold,
                        color: Colors.white,
                      )),
                ),
              ),
            )));
  }
}
