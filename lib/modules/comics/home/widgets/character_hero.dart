import 'package:actividad_05/models/comics/character.dart';
import 'package:actividad_05/models/comics/thumbnail.dart';
import 'package:flutter/material.dart';

class CharacterHero extends StatelessWidget {
  const CharacterHero({Key key, this.character, this.onTap}) : super(key: key);

  final Character character;
  final Function onTap;
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: () => onTap(),
        child: Container(
            width:
                thumbnailSizeDimensions[ThumbnailSize.LANDSCAPE_XLARGE].width,
            height:
                thumbnailSizeDimensions[ThumbnailSize.LANDSCAPE_XLARGE].height,
            margin: EdgeInsets.symmetric(horizontal: 10, vertical: 20),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(10),
              boxShadow: [
                BoxShadow(
                  color: Colors.black87,
                  blurRadius: 10,
                  offset: Offset(0, 4),
                ),
              ],
              image: DecorationImage(
                fit: BoxFit.fitWidth,
                image: NetworkImage(this
                    .character
                    .thumbnail
                    .getSizedThumb(ThumbnailSize.LANDSCAPE_XLARGE)),
              ),
            ),
            child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  gradient: LinearGradient(
                    begin: Alignment.bottomCenter,
                    end: Alignment.center,
                    colors: [
                      Colors.black,
                      Colors.black12,
                    ],
                  ),
                ),
                child: Row(children: [
                  Expanded(
                    child: Align(
                      alignment: FractionalOffset.bottomLeft,
                      child: Padding(
                        padding: EdgeInsets.only(left: 5, bottom: 5),
                        child: Text(character.name,
                            style: new TextStyle(
                              fontSize: 18.0,
                              color: Colors.white,
                            )),
                      ),
                    ),
                  ),
                ]))));
  }
}
