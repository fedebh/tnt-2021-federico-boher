import 'package:actividad_05/models/comics/character.dart';
import 'package:actividad_05/models/comics/thumbnail.dart';
import 'package:flutter/material.dart';
import 'character_hero.dart';

class CharacterHeroList extends StatelessWidget {
  const CharacterHeroList({Key key, this.list, this.onTap}) : super(key: key);

  final List<Character> list;
  final Function onTap;
  
  @override
  Widget build(BuildContext context) {
    return Container(
        height: thumbnailSizeDimensions[ThumbnailSize.LANDSCAPE_XLARGE].height,
        child: ListView.builder(
            scrollDirection: Axis.horizontal,
            itemCount: list.length,
            itemBuilder: (BuildContext context, int index) {
              return CharacterHero(
                  character: list[index], onTap: () => onTap(index));
            }));
  }
}
