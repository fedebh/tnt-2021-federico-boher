import 'package:actividad_05/models/comics/series.dart';
import 'package:actividad_05/models/comics/thumbnail.dart';
import 'package:actividad_05/modules/comics/home/widgets/series_hero.dart';
import 'package:flutter/material.dart';

class SeriesHeroList extends StatelessWidget {
    const SeriesHeroList({Key key, this.list, this.onTap}) : super(key: key);

  final List<Series> list;
  final Function onTap;

  @override
  Widget build(BuildContext context) {
    return  Container(
        height: thumbnailSizeDimensions[ThumbnailSize.LANDSCAPE_SMALL].height,
        child: ListView.builder(
            scrollDirection: Axis.horizontal,
            itemCount: list.length,
            itemBuilder: (BuildContext context, int index) {
              return SeriesHero(
                  series: list[index], onTap: () => onTap(index));
            }));
  }
}
