import 'package:actividad_05/bloc/marvel_api/marvel_api_bloc.dart';
import 'package:actividad_05/common/widgets/comics/thumb_hero_list\.dart';
import 'package:actividad_05/common/widgets/comics/marvel_attribution.dart';
import 'package:actividad_05/modules/comics/home/bloc/comics_home_screen_bloc.dart';
import 'package:actividad_05/modules/comics/home/widgets/character_hero_list.dart';
import 'package:actividad_05/modules/comics/home/widgets/series_hero_list.dart';
import 'package:actividad_05/routes/routes.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';

class ComicsHomeScreen extends StatelessWidget {
  const ComicsHomeScreen({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ComicsHomeScreenBloc, MarvelAPIState>(
        builder: (context, state) {
      if (state is MarvelAPILoadingState)
        return Scaffold(body: Center(child: CircularProgressIndicator()));

      if (state is MarvelAPILoadFailState)
        return Scaffold(body: Center(child: Text("Can't connect to server")));

      if (state is ComicsHomeScreenLoadedState) {
        return ListView(children: [
          CharacterHeroList(
              onTap: (index) => Get.toNamed(Routes.CHARACTER_DETAILS,
                  arguments: state.characters.data.results[index]),
              list: state.characters.data.results),
          SeriesHeroList(
              onTap: (index) => Get.toNamed(Routes.SERIES_DETAILS,
                  arguments: state.series.data.results[index]),
              list: state.series.data.results),
          ThumbHeroList(
              label: 'Latest Comics',
              onPressedIcon: () => Get.toNamed(Routes.COMICS_LATEST,
                  arguments: state.comics.data.results),
              onTapImage: (index) => Get.toNamed(Routes.COMICS_DETAILS,
                  arguments: state.comics.data.results[index]),
              list: state.comics.data.results),
          ThumbHeroList(
              label: 'Latest Events',
              onPressedIcon: () => Get.toNamed(Routes.EVENT_LATEST,
                  arguments: state.events.data.results),
              onTapImage: (index) => Get.toNamed(Routes.EVENT_DETAILS,
                  arguments: state.events.data.results[index]),
              list: state.events.data.results),
          ThumbHeroList(
              label: 'New Creators',
              onPressedIcon: () => Get.toNamed(Routes.CREATOR_LATEST,
                  arguments: state.creators.data.results),
              onTapImage: (index) => Get.toNamed(Routes.CREATOR_DETAILS,
                  arguments: state.creators.data.results[index]),
              list: state.creators.data.results),
          MarvelAttribution(),
        ]);
      }

      return Scaffold(body: Center(child: Text("Something went wrong")));
    });
  }
}
