import 'package:actividad_05/bloc/graphql/graphql_bloc.dart';
import 'package:actividad_05/services/graphql/anime_gql_service/queries/anime_gql_query.dart';
import 'package:actividad_05/models/anime/anime.dart';

class AnimeHomeBloc extends GraphQLBloc {
  AnimeHomeBloc({service}) : super(service: service);

  Map<String, List<dynamic>> mapList = {};

  List<Anime> _animeList(List<dynamic> list) {
    List<Anime> result = [];
    list.forEach((element) {
      result.add(Anime.cardFromJson(element));
    });
    return result;
  }

  @override
  Stream<GraphQLState> mapEventToState(GraphQLEvent event) async* {
    if (event is GraphQLFetchData) {
      yield* _mapFetchDataToStates(event);
    }
  }

  Stream<GraphQLState> _mapFetchDataToStates(GraphQLFetchData event) async* {
    final query = event.query;
    final variables = event.variables ?? null;

    try {
      final result = await service.performMutation(query, variables: variables);
      if (result.hasException) {
        print('graphQLErrors: ${result.exception.graphqlErrors.toString()}');
        yield GraphQLLoadFailState(result.exception.graphqlErrors[0]);
      } else {
        result.data.removeWhere((key, value) => key == '__typename');
        result.data.forEach((key, value) {
          mapList[key] = [];
          mapList[key].addAll(_animeList(value[ANIME_MEDIA_TYPE]));
        });
        yield GraphQLLoadSuccessState(mapList);
      }
    } catch (e) {
      print(e);
      yield GraphQLLoadFailState(e.toString());
    }
  }
}
