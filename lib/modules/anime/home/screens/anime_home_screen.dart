import 'package:actividad_05/bloc/graphql/graphql_bloc.dart';
import 'package:actividad_05/modules/anime/home/bloc/anime_home_bloc.dart';
import 'package:actividad_05/common/widgets/anime/anime_card_list\.dart';
import 'package:actividad_05/routes/routes.dart';
import 'package:actividad_05/utils/current_time.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';

class AnimeHomeScreen extends StatelessWidget {
  AnimeHomeScreen({Key key}) : super(key: key);

  onTapImage(index) => Get.toNamed(Routes.ANIME_DETAILS, arguments: index);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AnimeHomeBloc, GraphQLState>(builder: (context, state) {
      if (state is GraphQLLoadingState)
        return Scaffold(body: Center(child: CircularProgressIndicator()));

      if (state is GraphQLLoadSuccessState) {
        return ListView(
          children: [
            AnimeCardList(
                label: 'Trending NOW',
                onPressedIcon: () => Get.toNamed(Routes.ANIME_LIST, arguments: {
                      'type': 'trending',
                      'title': 'Trending NOW',
                      'data': state.data['trending'],
                      'variables': {'page': 1, 'perPage': 20}
                    }),
                onTapImage: (index) => Get.toNamed(Routes.ANIME_DETAILS,
                    arguments: state.data['trending'][index]),
                list: state.data['trending']),
            AnimeCardList(
                label: 'Popular this season',
                onPressedIcon: () => Get.toNamed(Routes.ANIME_LIST, arguments: {
                      'type': 'season',
                      'title': 'Popular this season',
                      'data': state.data['season'],
                      'variables': {
                        'page': 1,
                        'perPage': 20,
                        'season': CurrentTime.getCurrentSeason().toUpperCase(),
                        'seasonYear': CurrentTime.getCurrentYear(),
                      }
                    }),
                onTapImage: (index) => Get.toNamed(Routes.ANIME_DETAILS,
                    arguments: state.data['season'][index]),
                list: state.data['season']),
            AnimeCardList(
                label: 'Upcoming next season',
                onPressedIcon: () => Get.toNamed(Routes.ANIME_LIST, arguments: {
                      'type': 'nextSeason',
                      'title': 'Upcoming next season',
                      'data': state.data['nextSeason'],
                      'variables': {
                        'page': 1,
                        'perPage': 20,
                        'season': CurrentTime.getCurrentNextSeason().toUpperCase(),
                        'seasonYear': CurrentTime.getCurrentYear()
                      }
                    }),
                onTapImage: (index) => Get.toNamed(Routes.ANIME_DETAILS,
                    arguments: state.data['nextSeason'][index]),
                list: state.data['nextSeason']),
            AnimeCardList(
                label: 'All time popular',
                onPressedIcon: () => Get.toNamed(Routes.ANIME_LIST, arguments: {
                      'type': 'popular',
                      'title': 'All time popular',
                      'data': state.data['popular'],
                      'variables': {'page': 1, 'perPage': 20}
                    }),
                onTapImage: (index) => Get.toNamed(Routes.ANIME_DETAILS,
                    arguments: state.data['popular'][index]),
                list: state.data['popular']),
            AnimeCardList(
                label: 'Top',
                onPressedIcon: () => Get.toNamed(Routes.ANIME_LIST, arguments: {
                      'type': 'top',
                      'title': 'Top',
                      'data': state.data['top'],
                      'variables': {'page': 1, 'perPage': 20}
                    }),
                onTapImage: (index) => Get.toNamed(Routes.ANIME_DETAILS,
                    arguments: state.data['top'][index]),
                list: state.data['top']),
            SizedBox(height: 30),
          ],
        );
      }
      return SizedBox();
    });
  }
}
