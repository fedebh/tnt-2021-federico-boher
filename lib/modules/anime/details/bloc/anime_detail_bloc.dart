import 'package:actividad_05/bloc/graphql/graphql_bloc.dart';

class AnimeDetailBloc extends GraphQLBloc {
  AnimeDetailBloc({service}) : super(service: service);

  @override
  Stream<GraphQLState> mapEventToState(GraphQLEvent event) async* {
    if (event is GraphQLFetchData) {
      yield* _mapFetchDataToStates(event);
    }
  }

  Stream<GraphQLState> _mapFetchDataToStates(GraphQLFetchData event) async* {
    final query = event.query;
    final variables = event.variables ?? null;

    try {
      final result = await service.performMutation(query, variables: variables);

      if (result.hasException) {
        print('graphQLErrors: ${result.exception.graphqlErrors.toString()}');
        //print('clientErrors: ${result.exception.clientException.toString()}');
        yield GraphQLLoadFailState(result.exception.graphqlErrors[0]);
      } else {
        yield GraphQLLoadSuccessState(result.data);
      }
    } catch (e) {
      print(e);
      yield GraphQLLoadFailState(e.toString());
    }
  }
}
