import 'package:actividad_05/bloc/graphql/graphql_bloc.dart';
import 'package:actividad_05/common/templates/details/screens/detail_screen_template.dart';
import 'package:actividad_05/modules/anime/details/bloc/anime_detail_bloc.dart';
import 'package:actividad_05/services/graphql/anime_gql_service/anime_gql_service.dart';
import 'package:actividad_05/services/graphql/anime_gql_service/queries/anime_gql_query.dart';
import 'package:actividad_05/utils/structs.dart';
import 'package:actividad_05/models/anime/anime.dart';
import 'package:actividad_05/modules/anime/details/widgets/anime_bottom_modal_sheet.dart';
import 'package:actividad_05/modules/anime/details/widgets/anime_detail_list.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class AnimeDetailScreen extends StatelessWidget {
  const AnimeDetailScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Anime anime = ModalRoute.of(context).settings.arguments;
    return BlocProvider(
        create: (BuildContext context) => AnimeDetailBloc(
            service: context.read<AnimeGQLService>())
          ..add(GraphQLFetchData(ANIME_DETAILS_QUERY,
              variables: {'id': anime.id, 'type': 'ANIME', 'isAdult': false})),
        child: BlocBuilder<AnimeDetailBloc, GraphQLState>(
            builder: (context, state) {
          if (state is GraphQLLoadingState)
            return Scaffold(body: Center(child: CircularProgressIndicator()));

          if (state is GraphQLLoadSuccessState) {
            anime = Anime.fromJson(state.data['Media']);
            return DetailScreenTemplate<Anime>(
                entity: anime,
                header: Column(children: [
                  IconButton(
                    onPressed: () => {Navigator.of(context).pop()},
                    icon: Icon(Icons.arrow_back),
                    iconSize: 35.0,
                    color: Colors.black,
                  ),
                  Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(
                          bottomLeft: Radius.elliptical(60, 40),
                          bottomRight: Radius.elliptical(60, 40)),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.black87.withOpacity(0.9),
                          blurRadius: 0,
                        ),
                      ],
                      image: DecorationImage(
                        image: NetworkImage(anime.coverImage.extraLarge),
                        fit: BoxFit.cover,
                      ),
                    ),
                    width: double.infinity,
                    height: 500,
                  ),
                ]),
                title: anime.title.userPreferred,
                description: anime.description,
                share: Pair(animeBottomModalSheet, anime.externalLinks),
                relatedContentTitle: '',
                relatedContent: AnimeDetailList(anime));
          }

          return SizedBox();
        }));
  }
}
