import 'package:actividad_05/models/anime/anime.dart';
import 'package:flutter/material.dart';

class AnimeDetailList extends StatelessWidget {
  const AnimeDetailList(this.anime, {Key key}) : super(key: key);
  final Anime anime;

  Expanded listTile(String title, String subTitle) {
    return Expanded(
      child: ListTile(
        title: Text(title),
        subtitle: Text(subTitle),
      ),
    );
  }

  empty(title) => listTile(title, '-');

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          children: [
            anime.nextAiringEpisode == null
                ? empty('Airing')
                : listTile('Airing',
                    'Episode ' + anime.nextAiringEpisode.getDateMMddyyyy()),
            anime.format == null
                ? empty('Format')
                : listTile('Format', anime.format),
          ],
        ),
        Row(
          children: [
            anime.duration == null
                ? empty('Episode Duration')
                : listTile('Episode Duration', anime.duration.toString()),
            anime.status == null
                ? empty('Status')
                : listTile('Status', anime.status),
          ],
        ),
        Row(
          children: [
            anime.startDate == null
                ? empty('Start Date')
                : listTile('Start Date', anime.startDate.getDateMMddyyyy()),
            anime.season == null
                ? empty('Season')
                : listTile('Season', anime.season),
          ],
        ),
        Row(
          children: [
            anime.averageScore == null
                ? empty('Average Score')
                : listTile('Average Score', anime.averageScore.toString()),
            anime.meanScore == null
                ? empty('Mean Score')
                : listTile('Mean Score', anime.meanScore.toString()),
          ],
        ),
        Row(
          children: [
            anime.popularity == null
                ? empty('Popularity')
                : listTile('Popularity', anime.popularity.toString()),
            anime.favourites == null
                ? empty('Favorites')
                : listTile('Favorites', anime.favourites.toString()),
          ],
        ),
      ],
    );
  }
}
