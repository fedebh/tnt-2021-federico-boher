import 'package:flutter/material.dart';
import 'package:share_plus/share_plus.dart';

animeBottomModalSheet(context, links) {
  if(links == null) {
    return SnackBar(content: Text("No links to share"));
  }
  
  listTile(title, link) {
    return ListTile(
      leading: Icon(Icons.link),
      title: Text(title),
      onTap: () {
        Share.share(link);
        Navigator.pop(context);
      },
    );
  }

  return showModalBottomSheet(
      context: context,
      builder: (context) {
        return Wrap(
            children: links
                .map<ListTile>((item) => listTile(item.site, item.url))
                .toList());
      });
}
