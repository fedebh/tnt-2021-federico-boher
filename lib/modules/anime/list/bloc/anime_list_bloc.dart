import 'package:actividad_05/bloc/graphql/graphql_bloc.dart';
import 'package:actividad_05/services/graphql/anime_gql_service/queries/anime_gql_query.dart';
import 'package:actividad_05/models/anime/anime.dart';

class AnimeListBloc extends GraphQLBloc {
  AnimeListBloc({service}) : super(service: service);
  List<Anime> _animeList(List<dynamic> list) {
    List<Anime> result = [];
    list.forEach((element) {
      result.add(Anime.cardFromJson(element));
    });
    return result;
  }

  @override
  Stream<GraphQLState> mapEventToState(GraphQLEvent event) async* {
    if (event is GraphQLGetInitializedData) {
      yield* _mapGetInitializedDataToStates(event);
    } else if (event is GraphQLFetchPage) {
      yield* _mapFetchPageToStates(event);
    }
  }

  Stream<GraphQLState> _mapGetInitializedDataToStates(
      GraphQLGetInitializedData event) async* {
    yield GraphQLLoadingState();
    list = event.data;
    yield GraphQLLoadSuccessState(list);
  }

  Stream<GraphQLState> _mapFetchPageToStates(GraphQLFetchPage event) async* {
    yield GraphQLLoadingState();

    final type = event.type;
    final query = event.query;
    final variables = event.variables ?? null;
    variables['page'] = getNextPage();

    try {
      final result = await service.performMutation(query, variables: variables);

      if (result.hasException) {
        print('graphQLErrors: ${result.exception.graphqlErrors.toString()}');
        yield GraphQLLoadFailState(result.exception.graphqlErrors[0]);
      } else {
        list.addAll(_animeList(result.data[type][ANIME_MEDIA_TYPE]));
        yield GraphQLLoadSuccessState(list);

        toNextPage();
      }
    } catch (e) {
      print(e);
      yield GraphQLLoadFailState(e.toString());
    }
  }
}
