part of 'search_bloc.dart';

abstract class SearchState extends Equatable {
  const SearchState();

  @override
  List<Object> get props => null;
}

class SearchInitialState extends SearchState {}

class SearchTextEmptyState extends SearchState {}
class SearchEmptyState extends SearchState {}

class SearchFetchingState extends SearchState {}

class SearchFailureState extends SearchState {}

class SearchSuccessState extends SearchState {
  SearchSuccessState({this.characters, this.series, this.comics, this.events, this.creators, this.anime});

  final MarvelResponse<Character> characters;
  final MarvelResponse<Series> series;
  final MarvelResponse<Comic> comics;
  final MarvelResponse<MarvelEvent> events;
  final MarvelResponse<Creator> creators;
  final List<dynamic> anime;

  @override
  List<Object> get props => [characters, series, comics, events, creators, anime];
}
