part of 'search_bloc.dart';

abstract class SearchEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class SearchInitialize extends SearchEvent {}

class SearchClear extends SearchEvent {}

class SearchInputEntered extends SearchEvent {
  SearchInputEntered({this.searchText});
  final String searchText;

  @override
  List<Object> get props => [searchText];
}
