import 'dart:async';
import 'package:rxdart/rxdart.dart';
import 'package:actividad_05/bloc/marvel_api/marvel_api_bloc.dart';
import 'package:actividad_05/models/models.dart';
import 'package:actividad_05/repositories/marvel_repository.dart';
import 'package:actividad_05/services/graphql/anime_gql_service/anime_gql_service.dart';
import 'package:actividad_05/services/graphql/anime_gql_service/queries/anime_gql_query.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
part 'search_events.dart';
part 'search_state.dart';

class SearchBloc extends Bloc<SearchEvent, SearchState> {
  SearchBloc({@required this.marvelRepository, @required this.animeService})
      : super(SearchInitialState());

  final MarvelRepository marvelRepository;
  final AnimeGQLService animeService;
  List<bool> resultsStatus;
  String searchText;

  _clearStatus() {
    for (int i = 0; i < 6; i++) resultsStatus[i] = false;
  }

  List<Anime> _animeList(List<dynamic> list) {
    List<Anime> result = [];
    list.forEach((element) {
      result.add(Anime.cardFromJson(element));
    });
    return result;
  }

  // Se cancela el Event en ejecucion. Ejemplo: SearchFetchingState se cancela con SearchClear
  // https://github.com/felangel/bloc/issues/517
  // https://github.com/felangel/bloc/blob/daf09b994cbcc4b7105572d954c069e4795abbd0/examples/github_search/common_github_search/lib/src/github_search_bloc/github_search_bloc.dart#L23
  @override
  Stream<Transition<SearchEvent, SearchState>> transformEvents(
    Stream<SearchEvent> events,
    Stream<Transition<SearchEvent, SearchState>> Function(
      SearchEvent event,
    )
        transitionFn,
  ) {
    return events.switchMap(transitionFn);
  }

  @override
  Stream<SearchState> mapEventToState(SearchEvent event) async* {
    if (event is SearchInitialize) {
      searchText = '';
      resultsStatus = []..length = 6; // Uno para cada lista
      _clearStatus();
      yield SearchInitialState();
    } else if (event is SearchClear) {
      searchText = '';
      _clearStatus();
      yield SearchTextEmptyState();
    } else if (event is SearchInputEntered) {
      searchText = event.searchText?.trim() ?? '';
      if (searchText.isEmpty)
        yield SearchTextEmptyState();
      else
        yield SearchFetchingState();

      MarvelResponse<Character> characters =
          await _searchMarvelByStartsWith<Character>(searchText);
      MarvelResponse<Series> series =
          await _searchMarvelByStartsWith<Series>(searchText);
      MarvelResponse<Comic> comics =
          await _searchMarvelByStartsWith<Comic>(searchText);
      MarvelResponse<MarvelEvent> events =
          await _searchMarvelByStartsWith<MarvelEvent>(searchText);
      MarvelResponse<Creator> creators =
          await _searchMarvelByStartsWith<Creator>(searchText);

      List<dynamic> anime = [];
      try {
        final result = await animeService.performMutation(
            ANIME_SEARCH_BY_TEXT_QUERY,
            variables: {'page': 1, 'perPage': 20, 'searchText': searchText});

        if (!result.hasException)
          anime.addAll(
              _animeList(result.data[ANIME_SEARCH_TYPE][ANIME_MEDIA_TYPE]));
      } catch (e) {}

      if (anime.isEmpty &&
          MarvelAPIBloc.areEmptyResponses(
              [characters, series, comics, events, creators])) {
        yield SearchEmptyState();
        return;
      }

      yield SearchSuccessState(
          characters: characters,
          series: series,
          comics: comics,
          events: events,
          creators: creators,
          anime: anime);
          
      _clearStatus();
    }
  }

  Future<MarvelResponse<T>> _searchMarvelByStartsWith<T extends Entity>(
      startsWith) async {
    Map<String, String> _startsWith<T>(String startsWith) {
      switch (T) {
        case Character:
        case Creator:
        case MarvelEvent:
          return {'orderBy': '-modified', 'nameStartsWith': startsWith};
          break;
        case Series:
        case Comic:
          return {'orderBy': '-modified', 'titleStartsWith': startsWith};
          break;
        default:
          throw Exception('Type not supported');
      }
    }

    final response = await marvelRepository.getAll<T>(
        queryParams: _startsWith<T>(startsWith));
    return response;
  }
}
