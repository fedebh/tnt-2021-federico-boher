import 'package:actividad_05/common/widgets/anime/anime_card_list\.dart';
import 'package:actividad_05/common/widgets/comics/thumb_hero_list\.dart';
import 'package:actividad_05/modules/search/home/bloc/search_bloc.dart';
import 'package:actividad_05/routes/routes.dart';
import 'package:actividad_05/services/graphql/anime_gql_service/queries/anime_gql_query.dart';
import 'package:actividad_05/utils/helpers.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';

class SearchHomeScreen extends StatefulWidget {
  const SearchHomeScreen({Key key}) : super(key: key);

  @override
  _SearchHomeScreenState createState() => _SearchHomeScreenState();
}

class _SearchHomeScreenState extends State<SearchHomeScreen> {
  final Debouncer _debouncer = Debouncer(Duration(milliseconds: 300));
  FocusNode _searchFocusNode;
  TextEditingController _searchController;
  List<Widget> _resultsList = [];
  SearchBloc _bloc;

  @override
  void initState() {
    super.initState();
    _searchFocusNode = FocusNode();
    _searchController = TextEditingController()
      ..text = context.read<SearchBloc>().searchText;
  }

  @override
  void dispose() {
    _searchFocusNode.dispose();
    _searchController.dispose();
    _debouncer.dispose();
    super.dispose();
  }

  _onChangeSearchText(newText) {
    _resultsList.clear();
    _debouncer(() {
      context.read<SearchBloc>().add(SearchInputEntered(searchText: newText));
    });
  }

  _onSearchClearPressed() {
    _searchController.clear();
    _resultsList.clear();
    context.read<SearchBloc>().add(SearchClear());
  }

  _thumbHeroList(String title, String searchListRoute, String detailsRoute,
      List<dynamic> data, int listPosition) {
    String _searchText = _searchController.text;
    return ThumbHeroList(
        label: title,
        onPressedIcon: () => Get.toNamed(searchListRoute, arguments: {
              'title': "$title: $_searchText",
              'data': data,
              'status': _bloc.resultsStatus[listPosition],
              'searchText': _searchText
            })
              ..then((result) => _bloc.resultsStatus[listPosition] = result['status']),
        onTapImage: (index) =>
            Get.toNamed(detailsRoute, arguments: data[index]),
        list: data);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        floatingActionButton: FloatingActionButton(
          onPressed: () => _searchFocusNode.requestFocus(),
          child: const Icon(Icons.edit),
        ),
        body: GestureDetector(
            behavior: HitTestBehavior.translucent,
            onTap: () => _searchFocusNode.unfocus(),
            child: Column(
              children: [
                TextField(
                  controller: _searchController,
                  focusNode: _searchFocusNode,
                  onChanged: _onChangeSearchText,
                  decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      hintText: 'Search Comics and Anime',
                      suffixIcon: IconButton(
                        onPressed: () => _onSearchClearPressed(),
                        icon: Icon(Icons.clear),
                      )),
                ),
                BlocBuilder<SearchBloc, SearchState>(
                  builder: (context, state) {
                    _bloc = context.read<SearchBloc>();

                    if (state is SearchFetchingState) {
                      return Expanded(
                          child: Center(child: CircularProgressIndicator()));
                    }

                    if (state is SearchFailureState) return Text('ERROR');

                    if (state is SearchTextEmptyState) return SizedBox();

                    if (state is SearchSuccessState) {
                      if (state.characters.data.results.isNotEmpty) {
                        _resultsList.add(_thumbHeroList(
                            'Characters',
                            Routes.CHARACTER_SEARCH,
                            Routes.CHARACTER_DETAILS,
                            state.characters.data.results,
                            0));
                      }
                      if (state.series.data.results.isNotEmpty) {
                        _resultsList.add(_thumbHeroList(
                            'Series',
                            Routes.SERIES_SEARCH,
                            Routes.SERIES_DETAILS,
                            state.series.data.results,
                            1));
                      }
                      if (state.comics.data.results.isNotEmpty) {
                        _resultsList.add(_thumbHeroList(
                            'Comics',
                            Routes.COMICS_SEARCH,
                            Routes.COMICS_DETAILS,
                            state.comics.data.results,
                            2));
                      }
                      if (state.events.data.results.isNotEmpty) {
                        _resultsList.add(_thumbHeroList(
                            'Events',
                            Routes.EVENT_SEARCH,
                            Routes.EVENT_DETAILS,
                            state.events.data.results,
                            3));
                      }
                      if (state.creators.data.results.isNotEmpty) {
                        _resultsList.add(_thumbHeroList(
                            'Creators',
                            Routes.CREATOR_SEARCH,
                            Routes.CREATOR_DETAILS,
                            state.creators.data.results,
                            4));
                      }
                      if (state.anime.isNotEmpty) {
                        String _searchText = _searchController.text;
                        _resultsList.add(AnimeCardList(
                            label: 'Anime',
                            onTapImage: (index) => Get.toNamed(
                                Routes.ANIME_DETAILS,
                                arguments: state.anime[index]),
                            onPressedIcon: () => Get.toNamed(
                                    Routes.ANIME_SEARCH_LIST,
                                    arguments: {
                                      'title': "Anime: $_searchText",
                                      'type': ANIME_SEARCH_TYPE,
                                      'data': state.anime,
                                      'status': _bloc.resultsStatus[5],
                                      'variables': {
                                        'page': 1,
                                        'perPage': 20,
                                        'searchText': _searchText
                                      }
                                    })
                                  ..then((result) => _bloc.resultsStatus[5] =
                                      result['status']),
                            list: state.anime));
                      }

                      return Expanded(child: ListView(children: _resultsList));
                    }
                    return SizedBox();
                  },
                ),
              ],
            )));
  }
}
