import 'package:actividad_05/bloc/marvel_api/marvel_api_bloc.dart';
import 'package:actividad_05/common/widgets/snackbar_widget.dart';
import 'package:actividad_05/models/entity.dart';
import 'package:actividad_05/common/widgets/comics/thumb_hero_grid.dart';
import 'package:actividad_05/modules/search/list/bloc/comics_search_list_bloc.dart';
import 'package:actividad_05/repositories/marvel_repository.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';

class ComicsSearchListScreen<T extends Entity> extends StatelessWidget {
  const ComicsSearchListScreen({Key key, this.detailsRoute}) : super(key: key);
  final String detailsRoute;

  @override
  Widget build(BuildContext context) {
    final dynamic _args = ModalRoute.of(context).settings.arguments as dynamic;
    final String _title = _args['title'];
    final String _searchText = _args['searchText'];

    List<T> _data = _args['data'] as List<T>;
    bool _status = _args['status'];
    ComicsSearchListBloc _bloc;
    bool _isLoading;

    return BlocProvider(
        create: (BuildContext context) => ComicsSearchListBloc(
            repository: context.read<MarvelRepository>())
          ..add(
              MarvelAPIGetInitializedData({'status': _status, 'data': _data})),
        child: BlocBuilder<ComicsSearchListBloc, MarvelAPIState>(
            builder: (context, state) {
          _bloc = context.read<ComicsSearchListBloc>();
          _isLoading = state is MarvelAPILoadingState;

          if (state is MarvelAPILoadSuccessState) _data = state.data;

          return Stack(children: [
            Scaffold(
                appBar: AppBar(
                  iconTheme: IconThemeData(
                    color: Colors.black,
                  ),
                  leading: GestureDetector(
                      child: Icon(
                        Icons.arrow_back_ios,
                        color: Colors.black,
                      ),
                      onTap: () {
                        Get.back(result: {'status': _bloc.isEndReached});
                      }),
                  backgroundColor: Colors.transparent,
                  elevation: 0.0,
                  centerTitle: true,
                  title: Text(
                    _title ?? '',
                    style: TextStyle(color: Colors.black87),
                  ),
                ),
                backgroundColor: Colors.white,
                body: ThumbHeroGrid(
                    onEndReached: (endPosition) {
                      if (state is MarvelAPIEndPageReachedState) {
                        ScaffoldMessenger.of(context)
                            .showSnackBar(snackBarWidget('End Reached'));
                      } else {
                        _bloc
                          ..add(MarvelAPIFetchNextPage<T>(data: _searchText));
                      }
                    },
                    onTapImage: (index) =>
                        Get.toNamed(detailsRoute, arguments: _data[index]),
                    list: _data)),
            _isLoading
                ? Center(
                    child: Container(
                        child: CircularProgressIndicator(),
                        height: 60,
                        width: 60,
                        padding: const EdgeInsets.all(10),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(100),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.black54,
                              blurRadius: 6,
                              offset: Offset(0, 4),
                            ),
                          ],
                        )))
                : SizedBox()
          ]);
        }));
  }
}
