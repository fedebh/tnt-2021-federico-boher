import 'package:actividad_05/bloc/graphql/graphql_bloc.dart';
import 'package:actividad_05/common/widgets/anime/anime_card_grid.dart';
import 'package:actividad_05/common/widgets/snackbar_widget.dart';
import 'package:actividad_05/modules/search/list/bloc/anime_search_list_bloc.dart';

import 'package:actividad_05/routes/routes.dart';
import 'package:actividad_05/services/graphql/anime_gql_service/anime_gql_service.dart';
import 'package:actividad_05/services/graphql/anime_gql_service/queries/anime_gql_query.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';

class AnimeSearchListScreen extends StatelessWidget {
  const AnimeSearchListScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final dynamic _args = ModalRoute.of(context).settings.arguments as dynamic;
    final String _title = _args['title'];
    final Map<String, dynamic> _variables = _args['variables'];

    List<dynamic> _data = _args['data'];
    bool _status = _args['status'];
    AnimeSearchListBloc _bloc;
    bool _isLoading;

    return BlocProvider(
        create: (BuildContext context) => AnimeSearchListBloc(
            service: context.read<AnimeGQLService>())
          ..add(GraphQLGetInitializedData({'status': _status, 'data': _data})),
        child:
            BlocBuilder<AnimeSearchListBloc, GraphQLState>(builder: (context, state) {
          _bloc = context.read<AnimeSearchListBloc>();
          _isLoading = state is GraphQLLoadingState;

          if (state is GraphQLLoadSuccessState) _data = state.data;

          return Stack(children: [
            Scaffold(
                appBar: AppBar(
                  iconTheme: IconThemeData(
                    color: Colors.black,
                  ),
                  backgroundColor: Colors.transparent,
                  elevation: 0.0,
                  centerTitle: true,
                  title: Text(
                    _title ?? '',
                    style: TextStyle(color: Colors.black87),
                  ),
                  leading: GestureDetector(
                      child: Icon(
                        Icons.arrow_back_ios,
                        color: Colors.black,
                      ),
                      onTap: () {
                        Get.back(result: {'status': _bloc.isEndReached});
                      }),
                ),
                backgroundColor: Colors.white,
                body: AnimeCardGrid(
                    onEndReached: (endPosition) {
                      if (state is GraphQLEndPageReachedState) {
                        ScaffoldMessenger.of(context)
                            .showSnackBar(snackBarWidget('End Reached'));
                      } else {
                        _bloc
                          ..add(GraphQLFetchPage(ANIME_SEARCH_BY_TEXT_QUERY,
                              type: ANIME_SEARCH_TYPE, variables: _variables));
                      }
                    },
                    onTapImage: (index) => Get.toNamed(Routes.ANIME_DETAILS,
                        arguments: _data[index]),
                    list: _data)),
            _isLoading
                ? Center(
                    child: Container(
                        child: CircularProgressIndicator(),
                        height: 60,
                        width: 60,
                        padding: const EdgeInsets.all(10),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(100),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.black54,
                              blurRadius: 6,
                              offset: Offset(0, 4),
                            ),
                          ],
                        )))
                : SizedBox()
          ]);
        }));
  }
}
