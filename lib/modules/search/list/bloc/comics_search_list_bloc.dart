import 'package:rxdart/rxdart.dart';
import 'package:bloc/bloc.dart';
import 'package:actividad_05/bloc/marvel_api/marvel_api_bloc.dart';
import 'package:actividad_05/models/comics.dart';
import 'package:actividad_05/models/entity.dart';
import 'package:actividad_05/modules/comics/latests_list/bloc/latests_list_bloc.dart';

class ComicsSearchListBloc extends LatestsListBloc {
  ComicsSearchListBloc({repository}) : super(repository: repository);
  List<dynamic> list;

  @override
  Stream<Transition<MarvelAPIEvent, MarvelAPIState>> transformEvents(
    Stream<MarvelAPIEvent> events,
    Stream<Transition<MarvelAPIEvent, MarvelAPIState>> Function(
      MarvelAPIEvent event,
    )
        transitionFn,
  ) {
    return events.switchMap(transitionFn);
  }

  @override
  Stream<MarvelAPIState> mapEventToState(MarvelAPIEvent event) async* {
    if (event is MarvelAPIGetInitializedData)
      yield* _mapInitialDataToStates(event);
    else if (event is MarvelAPIFetchNextPage<Character>)
      yield* _mapFetchSearchToStates<Character>(event);
    else if (event is MarvelAPIFetchNextPage<Series>)
      yield* _mapFetchSearchToStates<Series>(event);
    else if (event is MarvelAPIFetchNextPage<Comic>)
      yield* _mapFetchSearchToStates<Comic>(event);
    else if (event is MarvelAPIFetchNextPage<MarvelEvent>)
      yield* _mapFetchSearchToStates<MarvelEvent>(event);
    else if (event is MarvelAPIFetchNextPage<Creator>)
      yield* _mapFetchSearchToStates<Creator>(event);
  }

  Stream<MarvelAPIState> _mapInitialDataToStates(
      MarvelAPIGetInitializedData event) async* {
    yield MarvelAPILoadingState();
    isEndReached = event.data['status'];
    list = event.data['data'] as List<dynamic>;
    yield MarvelAPILoadSuccessState(list);
    if (isEndReached) yield MarvelAPIEndPageReachedState();
  }

  Stream<MarvelAPIState> _mapFetchSearchToStates<T extends Entity>(
      MarvelAPIFetchNextPage<T> event) async* {
    if (isEndReached) {
      yield MarvelAPIEndPageReachedState();
      return;
    }
    yield MarvelAPILoadingState();
    final fetched = await repository.getAll<T>(
        queryParams: _toNextPageStartsWith<T>(event.data));

    list.addAll(fetched.data.results);
    yield MarvelAPILoadSuccessState(list);

    if (checkIfEndReached(fetched)) {
      isEndReached = true;
      yield MarvelAPIEndPageReachedState();
      return;
    }

    setPageOffset(fetched);
  }

  Map<String, String> _toNextPageStartsWith<T extends Entity>(
      String startsWith) {
    previousOffset = currentOffset;
    currentOffset += pageOffset;
    switch (T) {
      case Character:
      case Creator:
      case MarvelEvent:
        return {
          'orderBy': '-modified',
          'offset': currentOffset.toString(),
          'nameStartsWith': startsWith
        };
        break;
      case Series:
      case Comic:
        return {
          'orderBy': '-modified',
          'offset': currentOffset.toString(),
          'titleStartsWith': startsWith
        };
        break;
      default:
        throw Exception('Type not supported');
    }
  }
}
