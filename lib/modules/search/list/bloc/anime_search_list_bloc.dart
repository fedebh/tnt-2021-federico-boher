import 'package:actividad_05/bloc/graphql/graphql_bloc.dart';
import 'package:actividad_05/services/graphql/anime_gql_service/queries/anime_gql_query.dart';
import 'package:actividad_05/models/anime/anime.dart';
import 'package:actividad_05/models/anime/page_info.dart';

class AnimeSearchListBloc extends GraphQLBloc {
  AnimeSearchListBloc({service}) : super(service: service);
  bool isEndReached = false;

  List<Anime> _animeList(List<dynamic> list) {
    List<Anime> result = [];
    list.forEach((element) {
      result.add(Anime.cardFromJson(element));
    });
    return result;
  }

  @override
  Stream<GraphQLState> mapEventToState(GraphQLEvent event) async* {
    if (event is GraphQLGetInitializedData) {
      yield* _mapGetInitializedDataToStates(event);
    } else if (event is GraphQLFetchPage) {
      yield* _mapFetchPageToStates(event);
    }
  }

  Stream<GraphQLState> _mapGetInitializedDataToStates(
      GraphQLGetInitializedData event) async* {
    yield GraphQLLoadingState();
    list = event.data['data'];
    isEndReached = event.data['status'];
    yield GraphQLLoadSuccessState(list);
    if (isEndReached) yield GraphQLEndPageReachedState();
  }

  Stream<GraphQLState> _mapFetchPageToStates(GraphQLFetchPage event) async* {
    if (isEndReached) {
      yield GraphQLEndPageReachedState();
      return;
    }
    yield GraphQLLoadingState();

    final type = event.type;
    final query = event.query;
    final variables = event.variables ?? null;
    variables['page'] = getNextPage();

    AnimePageInfo pageInfo;

    try {
      final result = await service.performMutation(query, variables: variables);
      if (result == null) {
        isEndReached = true;
        yield GraphQLEndPageReachedState();
        return;
      }

      if (result.hasException) {
        print('graphQLErrors: ${result.exception.graphqlErrors.toString()}');
        yield GraphQLLoadFailState(result.exception.graphqlErrors[0]);
      } else {
        pageInfo =
            AnimePageInfo.fromJson(result.data[type][ANIME_PAGEINFO_TYPE]);

        list.addAll(_animeList(result.data[type][ANIME_MEDIA_TYPE]));
        yield GraphQLLoadSuccessState(list);

        if (!pageInfo.hasNextPage) {
          isEndReached = true;
          yield GraphQLEndPageReachedState();
          return;
        }

        toNextPage();
      }
    } catch (e) {
      print(e);
      yield GraphQLLoadFailState(e.toString());
    }
  }
}
