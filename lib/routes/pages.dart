part of 'routes.dart';

final List<GetPage> pages = [
  // ROOT
  GetPage(name: Routes.ROOT, page: () => AppRoot()),
  // OFFLINE
  GetPage(name: Routes.OFFLINE, page: () => OfflineRoot()),

  // COMICS
  // Details
  GetPage(name: Routes.CHARACTER_DETAILS, page: () => CharacterDetailScreen()),
  GetPage(name: Routes.SERIES_DETAILS, page: () => SeriesDetailScreen()),
  GetPage(name: Routes.COMICS_DETAILS, page: () => ComicsDetailScreen()),
  GetPage(name: Routes.EVENT_DETAILS, page: () => EventDetailScreen()),
  GetPage(name: Routes.CREATOR_DETAILS, page: () => CreatorDetailScreen()),

  // Search
  GetPage(
      name: Routes.CHARACTER_SEARCH,
      transition: Transition.leftToRight,
      page: () => ComicsSearchListScreen<Character>(
          detailsRoute: Routes.CHARACTER_DETAILS)),
  GetPage(
      name: Routes.SERIES_SEARCH,
      transition: Transition.leftToRight,
      page: () =>
          ComicsSearchListScreen<Series>(detailsRoute: Routes.SERIES_DETAILS)),
  GetPage(
      name: Routes.COMICS_SEARCH,
      transition: Transition.leftToRight,
      page: () =>
          ComicsSearchListScreen<Comic>(detailsRoute: Routes.COMICS_DETAILS)),
  GetPage(
      name: Routes.EVENT_SEARCH,
      transition: Transition.leftToRight,
      page: () => ComicsSearchListScreen<MarvelEvent>(
          detailsRoute: Routes.EVENT_DETAILS)),
  GetPage(
      name: Routes.CREATOR_SEARCH,
      transition: Transition.leftToRight,
      page: () => ComicsSearchListScreen<Creator>(
          detailsRoute: Routes.CREATOR_DETAILS)),

  // Latest
  GetPage(
      name: Routes.COMICS_LATEST,
      transition: Transition.leftToRight,
      page: () => LatestsListScreen<Comic>('Latest Comics',
          detailsRoute: Routes.COMICS_DETAILS)),
  GetPage(
      name: Routes.EVENT_LATEST,
      transition: Transition.leftToRight,
      page: () => LatestsListScreen<MarvelEvent>('Latest Events',
          detailsRoute: Routes.EVENT_DETAILS)),
  GetPage(
      name: Routes.CREATOR_LATEST,
      transition: Transition.leftToRight,
      page: () => LatestsListScreen<Creator>('New Creators',
          detailsRoute: Routes.CREATOR_DETAILS)),
  // Character Related
  GetPage(
      name: Routes.CHARACTER_RELATED_SERIES,
      transition: Transition.leftToRight,
      page: () => RelatedListScreen<Character, Series>()),
  GetPage(
      name: Routes.CHARACTER_RELATED_COMICS,
      transition: Transition.leftToRight,
      page: () => RelatedListScreen<Character, Comic>()),
  GetPage(
      name: Routes.CHARACTER_RELATED_EVENTS,
      transition: Transition.leftToRight,
      page: () => RelatedListScreen<Character, MarvelEvent>()),
  // Series Related
  GetPage(
      name: Routes.SERIES_RELATED_CHARACTERS,
      transition: Transition.leftToRight,
      page: () => RelatedListScreen<Series, Character>()),
  GetPage(
      name: Routes.SERIES_RELATED_COMICS,
      transition: Transition.leftToRight,
      page: () => RelatedListScreen<Series, Comic>()),
  GetPage(
      name: Routes.SERIES_RELATED_EVENTS,
      transition: Transition.leftToRight,
      page: () => RelatedListScreen<Series, MarvelEvent>()),
  GetPage(
      name: Routes.SERIES_RELATED_CREATORS,
      transition: Transition.leftToRight,
      page: () => RelatedListScreen<Series, Creator>()),
  // Comic Related
  GetPage(
      name: Routes.COMIC_RELATED_CHARACTERS,
      transition: Transition.leftToRight,
      page: () => RelatedListScreen<Comic, Series>()),
  GetPage(
      name: Routes.COMIC_RELATED_EVENTS,
      transition: Transition.leftToRight,
      page: () => RelatedListScreen<Comic, MarvelEvent>()),
  GetPage(
      name: Routes.COMIC_RELATED_CREATORS,
      transition: Transition.leftToRight,
      page: () => RelatedListScreen<Comic, Creator>()),
  // Event Related
  GetPage(
      name: Routes.EVENT_RELATED_CHARACTERS,
      transition: Transition.leftToRight,
      page: () => RelatedListScreen<MarvelEvent, Character>()),
  GetPage(
      name: Routes.EVENT_RELATED_SERIES,
      transition: Transition.leftToRight,
      page: () => RelatedListScreen<MarvelEvent, Series>()),
  GetPage(
      name: Routes.EVENT_RELATED_COMICS,
      transition: Transition.leftToRight,
      page: () => RelatedListScreen<MarvelEvent, Comic>()),
  GetPage(
      name: Routes.EVENT_RELATED_CREATORS,
      transition: Transition.leftToRight,
      page: () => RelatedListScreen<MarvelEvent, Creator>()),
  // Creator Related
  GetPage(
      name: Routes.CREATOR_RELATED_SERIES,
      transition: Transition.leftToRight,
      page: () => RelatedListScreen<Creator, Series>()),
  GetPage(
      name: Routes.CREATOR_RELATED_COMICS,
      transition: Transition.leftToRight,
      page: () => RelatedListScreen<Creator, Comic>()),
  GetPage(
      name: Routes.CREATOR_RELATED_EVENTS,
      transition: Transition.leftToRight,
      page: () => RelatedListScreen<Creator, MarvelEvent>()),

  // ANIME
  GetPage(name: Routes.ANIME_DETAILS, page: () => AnimeDetailScreen()),
  GetPage(
      name: Routes.ANIME_LIST,
      transition: Transition.leftToRight,
      page: () => AnimeListScreen()),
  GetPage(
      name: Routes.ANIME_SEARCH_LIST,
      transition: Transition.leftToRight,
      page: () => AnimeSearchListScreen()),

  // FAVORITES
  GetPage(
      name: Routes.FAVORITE_CHARACTER_LIST,
      transition: Transition.leftToRight,
      page: () => FavoriteComicsListScreen<Character>()),
  GetPage(
      name: Routes.FAVORITE_SERIES_LIST,
      transition: Transition.leftToRight,
      page: () => FavoriteComicsListScreen<Series>()),
  GetPage(
      name: Routes.FAVORITE_COMICS_LIST,
      transition: Transition.leftToRight,
      page: () => FavoriteComicsListScreen<Comic>()),
  GetPage(
      name: Routes.FAVORITE_EVENT_LIST,
      transition: Transition.leftToRight,
      page: () => FavoriteComicsListScreen<MarvelEvent>()),
  GetPage(
      name: Routes.FAVORITE_CREATOR_LIST,
      transition: Transition.leftToRight,
      page: () => FavoriteComicsListScreen<Creator>()),
  GetPage(
      name: Routes.FAVORITE_ANIME_LIST,
      transition: Transition.leftToRight,
      page: () => FavoriteAnimeListScreen()),
];
