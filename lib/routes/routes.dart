import 'package:actividad_05/modules/anime/list/screens/anime_list_screen.dart';
import 'package:actividad_05/modules/comics/related_list/screens/related_list_screen.dart';
import 'package:actividad_05/modules/favorites/list/screens/favorite_anime_list_screen.dart';
import 'package:actividad_05/modules/favorites/list/screens/favorite_comics_list_screen.dart';
import 'package:actividad_05/modules/root/app_root.dart';
import 'package:actividad_05/modules/root/offline_root.dart';
import 'package:actividad_05/modules/search/list/screens/anime_search_list_screen.dart';
import 'package:actividad_05/modules/search/list/screens/comics_search_list_screen.dart';
import 'package:get/get.dart' show GetPage, Transition;
import 'package:actividad_05/modules/comics/latests_list/screens/latests_list_screen.dart';
import 'package:actividad_05/models/comics.dart';
import 'package:actividad_05/modules/anime/details/screens/anime_detail_screen.dart';
import 'package:actividad_05/modules/comics/details/screens/character_detail_screen.dart';
import 'package:actividad_05/modules/comics/details/screens/comics_detail_screen.dart';
import 'package:actividad_05/modules/comics/details/screens/creator_detail_screen.dart';
import 'package:actividad_05/modules/comics/details/screens/event_detail_screen.dart';
import 'package:actividad_05/modules/comics/details/screens/series_detail_screen.dart';
part 'pages.dart';

class Routes {
  static const String ROOT = '/root';
  static const String OFFLINE = '/offline';

  // ============= COMICS
  // Details
  static const String _DETAILS = 'details';
  static const String CHARACTER_DETAILS = '/character/$_DETAILS';
  static const String SERIES_DETAILS = '/series/$_DETAILS';
  static const String COMICS_DETAILS = '/comics/$_DETAILS';
  static const String EVENT_DETAILS = '/event/$_DETAILS';
  static const String CREATOR_DETAILS = '/creator/$_DETAILS';
 
  // Latest
  static const String _LATEST = 'latest';
  static const String CHARACTER_LATEST = '/character/$_LATEST';
  static const String SERIES_LATEST = '/series/$_LATEST';
  static const String COMICS_LATEST = '/comics/$_LATEST';
  static const String EVENT_LATEST = '/event/$_LATEST';
  static const String CREATOR_LATEST = '/creator/$_LATEST';

  // Search
  static const String _SEARCH = 'search';
  static const String CHARACTER_SEARCH = '/character/$_SEARCH';
  static const String SERIES_SEARCH = '/series/$_SEARCH';
  static const String COMICS_SEARCH = '/comics/$_SEARCH';
  static const String EVENT_SEARCH = '/event/$_SEARCH';
  static const String CREATOR_SEARCH = '/creator/$_SEARCH';

  // Related
  static const _RELATED = 'related';
  // Character Related
  static const String _CHARACTER_RELATED = '/character/$_RELATED';
  static const String CHARACTER_RELATED_SERIES = '$_CHARACTER_RELATED/series';
  static const String CHARACTER_RELATED_COMICS = '$_CHARACTER_RELATED/comics';
  static const String CHARACTER_RELATED_EVENTS = '$_CHARACTER_RELATED/events';
  // Series Related
  static const String _SERIES_RELATED = '/series/$_RELATED';
  static const String SERIES_RELATED_CHARACTERS = '$_SERIES_RELATED/characters';
  static const String SERIES_RELATED_COMICS = '$_SERIES_RELATED/comics';
  static const String SERIES_RELATED_EVENTS = '$_SERIES_RELATED/events';
  static const String SERIES_RELATED_CREATORS = '$_SERIES_RELATED/creators';
  // Comic Related
  static const String _COMIC_RELATED = '/comic/$_RELATED';
  static const String COMIC_RELATED_CHARACTERS = '$_COMIC_RELATED/characters';
  static const String COMIC_RELATED_EVENTS = '$_COMIC_RELATED/events';
  static const String COMIC_RELATED_CREATORS = '$_COMIC_RELATED/creators';
  // Event Related
  static const String _EVENT_RELATED = '/event/$_RELATED';
  static const String EVENT_RELATED_CHARACTERS = '$_EVENT_RELATED/characters';
  static const String EVENT_RELATED_SERIES = '$_EVENT_RELATED/series';
  static const String EVENT_RELATED_COMICS = '$_EVENT_RELATED/comics';
  static const String EVENT_RELATED_CREATORS = '$_EVENT_RELATED/creators';
  // Creator Related
  static const String _CREATOR_RELATED = '/creator/$_RELATED';
  static const String CREATOR_RELATED_SERIES = '$_CREATOR_RELATED/series';
  static const String CREATOR_RELATED_COMICS = '$_CREATOR_RELATED/comics';
  static const String CREATOR_RELATED_EVENTS = '$_CREATOR_RELATED/events';

  // ============= ANIME
  static const String ANIME_DETAILS = '/anime/details';
  static const String ANIME_LIST = '/anime/list';
  static const String ANIME_SEARCH_LIST = '/anime/list/search';
  
  // ============= FAVORITES
  static const String FAVORITE_CHARACTER_LIST = '/favorite/character/list';
  static const String FAVORITE_SERIES_LIST = '/favorite/series/list';
  static const String FAVORITE_COMICS_LIST = '/favorite/comics/list';
  static const String FAVORITE_EVENT_LIST = '/favorite/event/list';
  static const String FAVORITE_CREATOR_LIST = '/favorite/creator/list';
  static const String FAVORITE_ANIME_LIST = '/favorite/anime/list';
}