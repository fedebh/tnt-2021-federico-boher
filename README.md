# My Comics&Anime
## Descripción
"My Comics&Anime" es una aplicación móvil para visualizar y gestionar comics (Marvel) y anime. <br />
Funcionalidades: <br />
- Visualizar los últimos personajes, series, comics, eventos y creadores de Marvel <br />
- Visualizar en detalle cada item listado anteriormente, además visualizar listas de contenido relacionado <br />
- Visualizar los anime en tendencia, los más populares en la temporada actual, los que estrenan en la proxima temporada <br />
- Visualizar en detalle cada anime <br />
- Compartir links de comics y anime <br />
- Buscar comics y anime <br />
- Gestionar la propia lista de comics y anime favoritos <br />
- Votar de 1 a 5 estrellas cada comic y anime <br />

## Entorno
Desarrollado en Flutter 2: <br />
- Los favoritos se almacenan localmente con SharedPreferences <br />
- Las estrellas se almacenan en la nube con Firebase Realtime Database. <br />

| Flutter version | Architecture |   Ref   | Release Date | Dart version |
|:---------------:|:------------:|:-------:|:------------:|:------------:|
| 2.10.5          | x64          | 5464c5b | 18/04/2022   | 2.16.2       |

### APIs
**Comics (Marvel)** <br />
[API REST](https://developer.marvel.com/docs) <br />
Marvel requiere de API keys, por lo tanto es necesario registrarse al servicio y agregar las claves obtenidas en un archivo .env: en el proyecto se encuentra un archivo .env.example que tiene que ser renombrado a .env

**Anime** <br />
[GraphQL](https://anilist.gitbook.io/anilist-apiv2-docs/) <br />
En este caso el acceso es publico, no se necesitan API keys.